# -*- coding: utf-8 -*-

import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time
import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

import openerp.addons.decimal_precision as dp

JOURNAL_TYPE_SELECTION = [
    ('sale', 'Sale'),
    ('sale_refund','Sale Refund'),
    ('purchase', 'Purchase'),
    ('purchase_refund','Purchase Refund'),
    ('cash', 'Cash'), ('bank', 'Bank and Checks'),
    ('general', 'General'),
    ('situation', 'Opening/Closing Situation')
]
class account_move(osv.osv):
    _inherit = "account.move"

    _columns = {
        'sale_date': fields.date('Sale Date'),
        'type_journal': fields.selection(JOURNAL_TYPE_SELECTION, 'Journal Type', size=32),
    }

    def oc_journal_type(self, cr, uid, ids, field, context=None):
        values = {}
        if field:
            obj = self.pool.get('account.journal').browse(cr, uid, field, context=context)
            values = {
                'type_journal' : obj.type,
                }
        return {'value' : values}


class account_move_line(osv.osv):
    _inherit = "account.move.line"

    _columns = {
        'sale_date': fields.related('move_id','sale_date', type='date', string='Sale Date', store=True),
        'type_journal': fields.related('move_id','type_journal', selection=JOURNAL_TYPE_SELECTION, type='selection', string='Journal Type'),
    }

   # vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
