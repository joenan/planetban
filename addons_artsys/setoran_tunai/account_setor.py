# -*- coding: utf-8 -*-

import logging
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time
import openerp
from openerp import SUPERUSER_ID, api
from openerp import tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
import json

import openerp.addons.decimal_precision as dp

JOURNAL_TYPE_SELECTION = [
    ('sale', 'Sale'),
    ('sale_refund','Sale Refund'),
    ('purchase', 'Purchase'),
    ('purchase_refund','Purchase Refund'),
    ('cash', 'Cash'), ('bank', 'Bank and Checks'),
    ('general', 'General'),
    ('situation', 'Opening/Closing Situation')
]

class account_setor(osv.osv):

    _name = "account.setor"
    _description = "Setoran Tunai"
    _rec_name = 'number'

    def _get_period(self, cr, uid, context=None):
        if context is None: context = {}
        if context.get('period_id', False):
            return context.get('period_id')
        periods = self.pool.get('account.period').find(cr, uid, context=context)
        return periods and periods[0] or False

    def get_concat_setor_tunai_id(self, cr, uid, ids, field_name, arg, context):
        x = {}
        for record in self.browse(cr, uid, ids ,context):
            x[record.id] = 'Setoran Tunai ID:' + ' ' + str(record.id or '')
        return x

    @api.model
    def get_pending_toko(self):
        res = {}
        result = []
        sql = """
            select distinct analytic_account_id from account_setor
            where state != 'stored'
            """
        self._cr.execute(sql)
        res = self._cr.dictfetchall()
        for hasil in res:
            result.append(hasil['analytic_account_id'])
        domain = [('id', 'not in', result)]
        return domain

    _columns = {
        'setor_date': fields.date('Date'),
        'name': fields.char('References'),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account'),
        'state': fields.selection([('draft', 'Draft'), ('generate', 'Generated'), ('stored', 'Stored')],'Status',readonly=True, select=True, copy=False,),
        'store_amount': fields.float('Amount'),
        'currency_id': fields.many2one('res.currency','Currency'),
        'user_id': fields.many2one('res.users','User'),
        'number': fields.char('Number', copy=False),
        'journal_id':fields.many2one('account.journal', 'Journal', readonly=True, states={'draft':[('readonly',False)]}),
        'account_debit_id':fields.many2one('account.account', 'Account Debit',  readonly=True, states={'draft':[('readonly',False)]}),
        'account_credit_id':fields.many2one('account.account', 'Account Credit',  readonly=True, states={'draft':[('readonly',False)]}),
        'period_id': fields.many2one('account.period','Period'),
        'company_id': fields.many2one('res.company','Compay',default=lambda self: self.env['res.company'].browse(self.env['res.company']._company_default_get('account.setor'))),
        'setor_tunai_id': fields.function(get_concat_setor_tunai_id, method=True, string='Reference',type='char', size=40),
        'sale_date': fields.date('Sale Date'),
        'type_journal': fields.selection(JOURNAL_TYPE_SELECTION, 'Journal Type', size=32),
        'payment_method': fields.many2one('account.journal', 'Payment Method', domain=[('journal_user','=', True)]),
        'line_ids': fields.one2many('account.setor.line','account_setor_id','Setor Line'),

    }
    _defaults = {
        'active': 1,
        'period_id': _get_period,
        'user_id': lambda s, cr, uid, c: uid,
        #'setor_date': date.today().strftime('%Y-%m-%d'),
        'setor_date': lambda *a:datetime.now().strftime('%Y-%m-%d'),
        'sale_date': date.today().strftime('%Y-%m-%d'),
        'payment_method': 7,
    }
    _order = "create_date desc, id desc"

    def copy(self, cr, uid, id, default=None, context=None):
        raise osv.except_osv(_('Forbbiden to duplicate'), _('Is not possible to duplicate the record, please create a new one.'))

    def unlink(self, cr, uid, ids, context=None):
        for line in self.browse(cr, uid, ids):
            if line.state == 'stored':
                raise osv.except_osv('error!', 'not allowed to delete record')
        return super(account_setor, self).unlink(cr, uid, ids)

    def oc_journal_type(self, cr, uid, ids, field, context=None):
        values = {}
        if field:
            obj = self.pool.get('account.journal').browse(cr, uid, field, context=context)
            values = {
                'type_journal' : obj.type,
                'account_debit_id': obj.default_debit_account_id.id,
                'account_credit_id': 823,
                }
        return {'value' : values}

    def check_double_toko(self, cr, uid, ids, context=None):
        setor_ids  = self.search(cr, uid, [('state','!=','stored')], context=context)
        setor_list = self.browse(cr, uid, setor_ids, context=context)
        return setor_list

    def create(self, cr, uid, vals, context=None):
        hasil = False
        if not vals:
            vals = {}
        if context is None:
            context = {}

        for toko in self.check_double_toko(cr, uid, vals, context):
            if vals['analytic_account_id'] == toko.analytic_account_id.id:
                hasil = True
        print 'vals: ' + str(hasil)

        if hasil:
            raise osv.except_osv('Warning!', 'Toko yang dimasukkan sudah di generate, tetapi belum di setor')
        else:
            vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'account.setor')
            vals['state'] = 'draft'
            return super(account_setor, self).create(cr, uid, vals, context=context)

    def _get_company_currency(self, cr, uid, setor_id, context=None):
        '''
        Get the currency of the actual company.

        :param setor_id: Id of the voucher what i want to obtain company currency.
        :return: currency id of the company of the voucher
        :rtype: int
        '''
        return self.pool.get('account.setor').browse(cr,uid,setor_id,context).journal_id.company_id.currency_id.id

    def _get_current_currency(self, cr, uid, setor_id, context=None):
        '''
        Get the currency of the voucher.

        :param setor_id: Id of the voucher what i want to obtain current currency.
        :return: currency id of the voucher
        :rtype: int
        '''
        setor = self.pool.get('account.setor').browse(cr,uid,setor_id,context)
        return setor.currency_id.id or self._get_company_currency(cr,uid,setor.id,context)

    # Journal Bank (Debit)
    def first_move_line_get(self, cr, uid, setor_id, setor_line_id, move_id, company_currency, current_currency, context=None):
        '''
        Return a dict to be use to create the first account move line of given 'setoran'.

        :param setor_id: Id of 'setoran' what we are creating account_move.
        :param move_id: Id of account move where this line will be added.
        :param company_currency: id of currency of the company to which the 'setoran' belong
        :param current_currency: id of currency of the 'setoran'
        :return: mapping between fieldname and value of account move line to create
        :rtype: dict
        '''
        setor = self.pool.get('account.setor').browse(cr,uid,setor_id,context)
        setor_line = self.pool.get('account.setor.line').browse(cr,uid,setor_line_id,context)
        debit = credit = 0.0
        # TODO: is there any other alternative then the 'setoran' type ??
        # ANSWER: We can have payment and receipt "In Advance".
        # TODO: Make this logic available.
        # -for sale, purchase we have but for the payment and receipt we do not have as based on the bank/cash journal we can not know its payment or receipt
        debit = setor_line.pencairan
        sign = debit - credit < 0 and -1 or 1
        #set the first line of the setoran
        move_line = {
                'name': setor_line.id or 'Setoran ' + str(setor_line.id or '') + ' #' + str(setor_line.payment_method.id or ''),
                'debit': debit,
                'credit': credit,
                'sale_date': setor_line.sale_date,
                'type_journal': setor.type_journal,
                'account_id': setor.account_debit_id.id,
                'move_id': move_id,
                'analytic_account_id': setor.analytic_account_id.id,
                'journal_id': setor.journal_id.id,
                'period_id': setor.period_id.id,
                'currency_id': company_currency <> current_currency and  current_currency or False,
                'amount_currency': (sign * abs(setor_line.pencairan) # amount < 0 for refunds
                    if company_currency != current_currency else 0.0),
                'date': setor.setor_date,
            }
        return move_line

    # Journal Lain-lain (Debit jika selisih plus + ) dan (credit jika selisih minus - )
    def first_move_line_get_lain(self, cr, uid, setor_id, setor_line_id, move_id, company_currency, current_currency, context=None):
        setor = self.pool.get('account.setor').browse(cr,uid,setor_id,context)
        setor_line = self.pool.get('account.setor.line').browse(cr,uid,setor_line_id,context)
        debit = credit = 0.0
        if (setor_line.biaya_lain > 0) :
            debit = setor_line.biaya_lain
        else:
            credit = abs(setor_line.biaya_lain)
        sign = debit - credit < 0 and -1 or 1
        move_line = {
                'name': 'Biaya Lain ' + str(setor_line.id or '') + ' #' + str(setor_line.payment_method.id or ''),
                'debit': debit,
                'credit': credit,
                'sale_date': setor_line.sale_date,
                'type_journal': setor.type_journal,
                'account_id': setor_line.account_biaya_lain.id,
                'move_id': move_id,
                'analytic_account_id': setor.analytic_account_id.id,
                'journal_id': setor.journal_id.id,
                'period_id': setor.period_id.id,
                'currency_id': company_currency <> current_currency and  current_currency or False,
                'amount_currency': (sign * abs(setor_line.biaya_lain) # amount < 0 for refunds
                    if company_currency != current_currency else 0.0),
                'date': setor.setor_date,
            }
        return move_line

    def account_move_get(self, cr, uid, setor_id, setor_line_id, context=None):
        seq_obj = self.pool.get('ir.sequence')
        setor = self.pool.get('account.setor').browse(cr,uid,setor_id,context)
        setor_line = self.pool.get('account.setor.line').browse(cr,uid,setor_line_id,context)
        if setor.journal_id.sequence_id:
            if not setor.journal_id.sequence_id.active:
                raise osv.except_osv(_('Configuration Error !'),
                    _('Please activate the sequence of selected journal !'))
            c = dict(context)
            c.update({'fiscalyear_id': setor.period_id.fiscalyear_id.id})
            name = seq_obj.next_by_id(cr, uid, setor.journal_id.sequence_id.id, context=c)
        else:
            raise osv.except_osv(_('Error!'),
                        _('Please define a sequence on the journal.'))


        move = {
            'name': name,
            #'setor_id': setor.id,
            'journal_id': setor.journal_id.id,
            'date': setor.setor_date,
            'state':'posted',
            'sale_date': setor_line.sale_date,
            'type_journal': setor.type_journal,
            'analytic_account_id': setor.analytic_account_id.id,
            #'ref': setor.setor_tunai_id,
            'ref': setor.number,
            'period_id': setor.period_id.id,
        }
        return move

    def _convert_amount(self, cr, uid, amount, setor_id, setor_line_id, context=None):
        '''
        This function convert the amount given in company currency. It takes either the rate in the voucher (if the
        payment_rate_currency_id is relevant) either the rate encoded in the system.

        :param amount: float. The amount to convert
        :param voucher: id of the voucher on which we want the conversion
        :param context: to context to use for the conversion. It may contain the key 'date' set to the voucher date
            field in order to select the good rate to use.
        :return: the amount in the currency of the voucher's company
        :rtype: float
        '''
        if context is None:
            context = {}
        currency_obj = self.pool.get('res.currency')
        setor_line = self.pool.get('account.setor.line').browse(cr, uid, setor_line_id, context=context)
        setor = self.browse(cr, uid, setor_id, context=context)
        return currency_obj.compute(cr, uid, setor.currency_id.id,setor.company_id.currency_id.id, amount, context=context)

    # piutang usaha PB (Credit)
    def setor_move_line_create(self, cr, uid, setor_id, setor_line_id, line_total, move_id, company_currency, current_currency, context=None):
        '''
        Create one account move line, on the given account move, per voucher line where amount is not 0.0.
        It returns Tuple with tot_line what is total of difference between debit and credit and
        a list of lists with ids to be reconciled with this format (total_deb_cred,list_of_lists).

        :param voucher_id: Voucher id what we are working with
        :param line_total: Amount of the first line, which correspond to the amount we should totally split among all voucher lines.
        :param move_id: Account move wher those lines will be joined.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: Tuple build as (remaining amount not allocated on voucher lines, list of account_move_line created in this method)
        :rtype: tuple(float, list of int)
        '''
        if context is None:
            context = {}
        move_line_obj = self.pool.get('account.move.line')
        currency_obj = self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        tot_line = line_total
        rec_lst_ids = []

        date = self.read(cr, uid, [setor_id], ['setor_date'], context=context)[0]['setor_date']
        ctx = context.copy()
        ctx.update({'date': date})

        setor = self.pool.get('account.setor').browse(cr, uid, setor_id, context)
        setor_line = self.pool.get('account.setor.line').browse(cr, uid, setor_line_id, context)
        setor_currency = setor.journal_id.currency
        amount = self._convert_amount(cr, uid, setor_line.pencairan, setor.id, setor_line.id, context=ctx)
        currency_rate_difference = 0.0
        move_line = {
            'journal_id': setor.journal_id.id,
            'period_id': setor.period_id.id,
            'name': 'Setoran ' + str(setor_line.id or '') + ' #' + str(setor_line.payment_method.id or ''),
            'account_id': setor.account_credit_id.id,
            'sale_date': setor_line.sale_date,
            'type_journal': setor.type_journal,
            'move_id': move_id,
            'partner_id': False,
            'currency_id': False,
            'analytic_account_id': setor.analytic_account_id.id,
            'quantity': 0,
            'credit': 0.0,
            'debit': 0.0,
            'date': setor.setor_date
        }
        if amount < 0:
            amount = -amount
        tot_line += amount
        if setor_line.biaya_lain:
            if setor_line.account_biaya_lain:
                amount = amount + setor_line.biaya_lain
        move_line['credit'] = amount
        #else:
        foreign_currency_diff = 0.0
        amount_currency = False
        move_line['amount_currency'] = amount_currency
        voucher_line = move_line_obj.create(cr, uid, move_line)
        #if not currency_obj.is_zero(cr, uid, setor.company_id.currency_id, currency_rate_difference):
        #    # Change difference entry in company currency
        #    exch_lines = self._get_exchange_lines(cr, uid, move_id, company_currency, current_currency, context=context)
        #    new_id = move_line_obj.create(cr, uid, exch_lines[0],context)
        #    move_line_obj.create(cr, uid, exch_lines[1], context)


    def _sel_context(self, cr, uid, setor_id, context=None):
        """
        Select the context to use accordingly if it needs to be multicurrency or not.

        :param voucher_id: Id of the actual voucher
        :return: The returned context will be the same as given in parameter if the voucher currency is the same
                 than the company currency, otherwise it's a copy of the parameter with an extra key 'date' containing
                 the date of the voucher.
        :rtype: dict
        """
        company_currency = self._get_company_currency(cr, uid, setor_id, context)
        current_currency = self._get_current_currency(cr, uid, setor_id, context)
        if current_currency <> company_currency:
            context_multi_currency = context.copy()
            setor = self.pool.get('account.setor').browse(cr, uid, setor_id, context)
            context_multi_currency.update({'date': setor.date})
            return context_multi_currency
        return context

    def writeoff_move_line_get(self, cr, uid, setor_id, line_total, move_id, name, company_currency, current_currency, context=None):
        '''
        Set a dict to be use to create the writeoff move line.

        :param voucher_id: Id of voucher what we are creating account_move.
        :param line_total: Amount remaining to be allocated on lines.
        :param move_id: Id of account move where this line will be added.
        :param name: Description of account move line.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: mapping between fieldname and value of account move line to create
        :rtype: dict
        '''
        currency_obj = self.pool.get('res.currency')
        move_line = {}

        setor = self.pool.get('account.setor').browse(cr,uid,setor_id,context)
        current_currency_obj = setor.currency_id or setor.journal_id.company_id.currency_id

        return move_line


    #not fixed yet
    def action_move_line_create(self, cr, uid, ids, context=None):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        '''
        if context is None:
            context = {}
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for setor in self.browse(cr, uid, ids, context=context):
            for setor_line in setor.line_ids:
                local_context = dict(context, force_company=setor.journal_id.company_id.id)
                company_currency = self._get_company_currency(cr, uid, setor.id, context)
                current_currency = self._get_current_currency(cr, uid, setor.id, context)

                # we select the context to use accordingly if it's a multicurrency case or not
                context = self._sel_context(cr, uid, setor.id, context)
                # But for the operations made by _convert_amount, we always need to give the date in the context
                ctx = context.copy()
                ctx.update({'date': setor.setor_date})

                # Create the account move record.
                move_id = move_pool.create(cr, uid, self.account_move_get(cr, uid, setor.id, setor_line.id, context=context), context=context)
                name = move_pool.browse(cr, uid, move_id, context=context).name
                print name

                # Create the first line of Account_Move_Line
                move_line_id = move_line_pool.create(cr, uid, self.first_move_line_get(cr,uid,setor.id, setor_line.id, move_id, company_currency, current_currency, local_context), local_context)
                move_line_brw = move_line_pool.browse(cr, uid, move_line_id, context=context)

                # Create second
                line_total = move_line_brw.debit - move_line_brw.credit
                rec_list_ids = self.setor_move_line_create(cr, uid, setor.id, setor_line.id, line_total, move_id, company_currency, current_currency, context)

                # Create Account move line for Biaya Lain
                if setor_line.account_biaya_lain:
                    move_line_lain_id = move_line_pool.create(cr, uid, self.first_move_line_get_lain(cr,uid,setor.id, setor_line.id, move_id, company_currency, current_currency, local_context), local_context)

                # Create the writeoff line if needed
                #ml_writeoff = self.writeoff_move_line_get(cr, uid, setor.id, line_total, move_id, name, company_currency, current_currency, local_context)
                #if ml_writeoff:
                #    move_line_pool.create(cr, uid, ml_writeoff, local_context)
                # We post the voucher.
            #self.write(cr, uid, [setor.id], {
                #'move_id': move_id,
                #'state': 'stored',
                #'number': name,
            #})

                #if setor.journal_id.entry_posted:
                #    move_pool.post(cr, uid, [move_id], context={})
                # We automatically reconcile the account move lines.
            reconcile = False
        return True


    #@api.multi
    def setor(self, cr, uid, ids, context=None):
        warning = False
        for setor in self.browse(cr, uid, ids, context=None):
            for setor_line in setor.line_ids:
                if setor_line.biaya_lain:
                    if not setor_line.account_biaya_lain:
                        warning = True

        if warning:
            raise osv.except_osv('Warning!', 'Account biaya lain belum dipilih')
        else:
            obj_seq = self.pool.get('ir.sequence')
            numbering = obj_seq.next_by_code(cr, uid, 'setoran.tunai', context=context)
            self.write(cr, uid, ids, {
                'number': numbering,
                'state': 'stored',
                })
            self.action_move_line_create(cr, uid, ids, context=context)

    def oc_analytic_account(self, cr, uid, ids, field, context=None):
        values = {}
        if field:
            obj = self.pool.get('res.users').browse(cr, uid, field, context=context)
            values = {
                'analytic_account_id' : obj.pos_config.analytic_account_id.id,
               }
        return {'value' : values}

    def _oc_get_ending_balance(self, cr, uid, ids, analytic_account, sale_date, payment, context=None):
        values = {}
        amount = 0
        if analytic_account:
            if payment:
                sql = """
                    SELECT
                        coalesce(sum(cash-transfer),0)	as beginning_balance
                    FROM
                    ((select a.analytic_account_id,
                                       case when a.journal_id=%s then sum(a.balance_end) end as cash,
                                       0 as transfer
                                       from account_bank_statement as a
                                       where a.state='confirm' and a.balance_end<>0 and a.date::date = %s
                                       group by a.analytic_account_id,a.date,a.journal_id,company_id
                                       order by a.analytic_account_id,a.date,a.journal_id)
                                UNION ALL
                                -----Mencari transaksi dari Kas Besar yang di transfer ke rekening pusat
                                (select a.analytic_account_id,
                                    0 as cash,
                                    sum(a.credit) as transfer
                                    from account_move_line a
                                    left join account_move b on a.move_id=b.id
                                    where a.account_id=823 and a.credit<>0 and b.sale_date::date = %s
                                    and b.state='posted' and a.name ilike %s
                                    group by a.analytic_account_id,b.sale_date,a.company_id
                                    order by a.analytic_account_id,b.sale_date))	AS tbl_beginning
                    Where analytic_account_id=%s
                """
                cr.execute(sql, (payment, sale_date, sale_date, '%#'+str(payment), analytic_account))
                res = cr.fetchone()
                print res
                if res:
                    amount = res[0]

                if sale_date:
                    values = {
                        'store_amount': amount,
                    }
        return {'value': values}

    def generate_line(self, cr, uid, ids, context=None):
        res = {}
        obj_setor_line = self.pool.get('account.setor.line')
        if ids:
            for setor in self.browse(cr, uid, ids, context=None):
                self.write(cr, uid, [setor.id], {'state': 'generate'})
                sql = """
                SELECT
                        """ + str(setor.id)+ """ as account_setor_id,
                        date as sale_date,
                        journal_id as payment_method,
                        coalesce(sum(sales),0)	as sales,
                        coalesce(sum(transfer),0)	as transfer,
                        coalesce(sum(sales) - sum(transfer),0) as piutang
                    FROM
                    (
                    -----Mencari nilai pembayaran berdasarkan payment methode.
                    (select a.analytic_account_id,
                           a.date,
                           0 as beginning,
                           journal_id::text,
                           sum(a.balance_end) as sales,
                           0 as transfer,
                           company_id
                           from account_bank_statement as a
                           where a.state='confirm' and a.balance_end<>0
                           group by a.analytic_account_id,a.date,a.journal_id,company_id
                           order by a.analytic_account_id,a.date,a.journal_id)
                    UNION ALL
                    -----Mencari transaksi dari Kas Besar yang di transfer ke rekening pusat
                    (select a.analytic_account_id,
                           a.sale_date,
                           0 as beginning,
                           (substring(a.name,position('#' IN a.name)+1)) as journal_id,
                           0 as sales,
                           sum(a.credit) as transfer,
                           a.company_id
                          from account_move_line a
                          left join account_move b on a.move_id=b.id
                          where a.account_id=823 and a.credit<>0 and a.name ilike %s
                          and b.state='posted'
                          group by a.analytic_account_id,a.sale_date,a.company_id,substring(a.name,position('#' IN a.name)+1)
                          order by a.analytic_account_id,a.sale_date)) as data
                    WHERE analytic_account_id=%s and date is not null
                    GROUP BY date,journal_id
                    having sum(sales) - sum(transfer) <> 0
                    ORDER BY date
                """
                cr.execute(sql, ('%#%',setor.analytic_account_id.id,))
                res = cr.dictfetchall()
                vals = {}
                no=0
                for hasil in res:
                    no += 1
                    vals = obj_setor_line.create(cr, uid, {
                            'account_setor_id': ids[0],
                            'sale_date': hasil['sale_date'],
                            'payment_method': hasil['payment_method'],
                            'piutang': hasil['piutang'],
                            'pencairan': hasil['piutang'],
                    })
                    print no
            return vals

class account_setor_line(osv.osv):
    _name = "account.setor.line"
    _description = "Setoran Tunai Line"

    _columns = {
        'account_setor_id': fields.many2one('account.setor','Setor ID', ondelete="cascade"),
        'sale_date': fields.date('Tanggal'),
        'payment_method': fields.many2one('account.journal', 'Payment Method', domain=[('journal_user','=', True)]),
        'piutang': fields.float('Piutang'),
        'pencairan': fields.float('Pencairan'),
        'selisih': fields.float('Selisih'),
        'account_biaya_lain': fields.many2one('account.account', 'Account Biaya Lain'),
        'biaya_lain': fields.float('Biaya Lain'),
    }

    def _hitung_selisih(self, cr, uid, ids, piutang, pencairan, context=None):
        values = {}
        if piutang:
            values = {
                'selisih' : piutang - pencairan,
                'biaya_lain' : piutang - pencairan,
                }
        return {'value' : values}

    def oc_biaya_lain(self, cr, uid, ids, piutang, pencairan, biaya_lain, context=None):
        values = {}
        if pencairan:
            values = {
                'selisih' : piutang - pencairan
            }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
