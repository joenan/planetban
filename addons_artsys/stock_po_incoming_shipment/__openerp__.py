# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Stock PO Incoming Shipment',
    'version': '1.1',
    'website': 'https://www.artsys.id',
    'author' : 'Artsys',
    'category': 'Inventory Management',
    'sequence': 10,
    'summary': 'Stock Purchase Order (PO) Incoming',
    'depends': [
        'stock',
        'purchase',
    ],
    'description': """
    """,
    'data': [
        'stock_view.xml',
    ],
    'qweb': ['static/src/xml/picking.xml'],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
