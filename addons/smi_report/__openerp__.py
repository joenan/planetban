{
'name':'SMI Report',
'version':'1.0',
'author':'SMI Project',
'category':'Hidden',
'summary':'Custom module for internal use.',
'description': """
===============================
""",
'depends':['point_of_sale'],
'data':['views/report_pos_sales.xml',
        'views/report_pos_pricelist.xml',
        'data/actions.xml',
        'data/menu.xml',
        'report/report_template.xml',
        'report/report_pos_sales.xml',
        'report/report_pos_pricelist.xml'],
'license': 'Other proprietary',
'application': False,
'auto_install':False,
}