from openerp import models,fields,api,exceptions
import logging
_logger=logging.getLogger(__name__)

class RWPosPricelist(models.TransientModel):
    _name = 'smi.report.wizard'
    _inherit = 'smi.report.wizard'
    
    @api.multi
    def btn_pos_pricelist_qty_report(self):
        self.ensure_one()
        self.write({'usage':'Stock Balance and Pricelist Report',})
        records = self.read()[0]
        data = {
            'usage': 'QP',
            'location_id': records['location_id'],
            'date_to': records['date_end']
            }
        return self.env['report'].get_action(self,'smi_report.pos_report_pricelist_qty', data)
    
class PosReportPricelitsQty(models.AbstractModel):
    _name = 'report.smi_report.pos_report_pricelist_qty'
    
    @api.multi
    def render_html(self,data=None,context=None):
        if data['location_id']==False or data['date_to']==False:
            _logger.error('Required document not set in Stock Balance and Pricelist Report!.')
            #raise exceptions.Warning('Required document not set in POS Report Sales!.')            
        else:
            report=self.env['report']
            document=self._get_pos_report_pricelist_qty(data)
            ctx=self._context.copy()
            object=self.env['smi.report.wizard'].browse([ctx['active_id']])
            data={'o':object,
                  'docs':document}
            return report.render('smi_report.pos_report_pricelist_qty', data)
    
    def _get_pos_report_pricelist_qty(self,args):
        L=args['location_id'][0]
        D2=args['date_to']
        sql="""
            select
                brand,
                code,
                product_name,
                uom,
                status as state,
                sum(ending_qty) as ending_qty,
                sum(list_price) as list_price,
                sum(price_surcharge) as price_surcharge,
                (sum(list_price) - -sum(price_surcharge)) as sale_price
            from(
                -- Ending Balance-----------------------------------------
                (select
                    g.name as brand,
                    d.default_code as code,
                    d.name_template as product_name,
                    e.name as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    sum(case 
                            when (c.usage='internal' and a.location_dest_id=%s) --L1
                                then product_uom_qty else - product_uom_qty end) as ending_qty,
                    0 as list_price,
                    0 as price_surcharge    
                from
                    stock_move as a
                    left join stock_location as b on a.location_id=b.id
                    left join stock_location as c on a.location_dest_id=c.id
                    left join product_product as d on a.product_id=d.id
                    left join product_template as f on d.product_tmpl_id=f.id
                    left join product_uom as e on f.uom_id=e.id
                    left join product_tire_brand as g on f.pb_tire_brand=g.id
                where
                    a.state='done'
                    and (a.location_id=%s or a.location_dest_id=%s) --L2 L3
                    and a.date::date <=%s --D4
                group by g.name,d.default_code,d.name_template,e.name,d.active
                order by g.name,d.default_code,d.name_template)

                union all
                -- List_price and Discount -----------------------------------------------------
                (select
                    g.name as brand,
                    d.default_code as code,
                    d.name_template as product_name,
                    e.name as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0 as ending_qty,
                    f.list_price as list_price,
                    ppi.price_surcharge as price_surcharge
                from 
                    product_pricelist_item as ppi
                    left join product_product as d on ppi.product_id=d.id
                    left join product_template as f on d.product_tmpl_id=f.id
                    left join product_uom as e on f.uom_id=e.id
                    left join product_tire_brand as g on f.pb_tire_brand=g.id
                    left join product_pricelist_version as ppv on ppv.id=ppi.price_version_id
                    left join product_pricelist as pp on pp.id=ppv.pricelist_id
                    left join pos_config as pc on pc.pricelist_id=pp.id
                where 
                    pc.stock_location_id=%s --L5
                    and pp.active is true
                    and ppv.active is true
                group by g.name,d.default_code,d.name_template,e.name,d.active,f.list_price,ppi.price_surcharge
                order by g.name,d.default_code,d.name_template)
                )as data
            group by brand,code,product_name,uom,state
            order by brand,code,product_name
        """
        self.env.cr.execute(sql,(L,L,L,D2,
                                 L))
        res=self.env.cr.dictfetchall()
        return res