from openerp import models,fields,api,exceptions
import logging
_logger=logging.getLogger(__name__)

class SmiReportWizard(models.TransientModel):
    _name='smi.report.wizard'
    company_id=fields.Many2one('res.company',string='Company',
                               default=lambda self:self.env.user.company_id)
    location_id=fields.Many2one('stock.location',string='WH Location',
                                default=lambda self:self.env.user.pos_config.stock_location_id)
    date_start=fields.Date(string='Start Date')
    date_end=fields.Date(string='End Date')
    usage=fields.Char('Usage')
        
    @api.multi
    def btn_pos_report_sales(self):
        self.ensure_one()
        self.write({'usage':'POS Report Sales',})
        records = self.read()[0]
        data = {
            'location_id': records['location_id'],
            'date_start': records['date_start'],
            'date_end': records['date_end']
            }
        return self.env['report'].get_action(self,'smi_report.pos_report_sales', data)
        
class PosReportSales(models.AbstractModel):
    _name = 'report.smi_report.pos_report_sales'
    
    @api.multi
    def render_html(self,data=None,context=None):
        if data['location_id']==False or data['date_end']==False:
            _logger.error('Required document not set in POS Report Sales!.')
            #raise exceptions.Warning('Required document not set in POS Report Sales!.')            
        else:
            report=self.env['report']
            document=self._get_pos_report_sales(data)
            ctx=self._context.copy()
            object=self.env['smi.report.wizard'].browse([ctx['active_id']])
            data={'o':object,
                  'docs':document}
            return report.render('smi_report.pos_report_sales', data)
    
    def _get_pos_report_sales(self,args):
        L=args['location_id'][0]
        D1=args['date_start']
        D2=args['date_end']
        if D1==False:
            D1=D2
        sql="""
            SELECT
                    brand,
                    code,
                    product_name,
                    uom,
                    status as state,
                    sum(v_begining) as begining,
                    sum(v_sales_return) as sales_return,
                    sum(v_sales) as sales,
                    sum(v_pos_return) as pos_return,
                    sum(v_pos)                        as pos_sales,
                    sum(begining) as qty_begining,
                    sum(sales_return) as qty_sales_return,
                    sum(sales) as qty_sales,
                    sum(pos) as qty_pos_sales,
                    sum(pos_return) as qty_pos_return
    
            FROM    (
                        -- Begining Balance
                        (select
                                g.name as brand,
                                d.default_code as code,
                                d.name_template as product_name,
                                e.name as uom,
                                case 
                                    when d.active='t'
                                        then 'Active'
                                    else 'Non Active' end as status,
                                sum(case
                                        when (c.usage='internal' and a.location_dest_id= %s) --L1 
                                            then product_uom_qty 
                                        else -product_uom_qty end) as begining,
                                sum(case 
                                        when (c.usage='internal' and a.location_dest_id= %s) --L2
                                            then price_unit*product_uom_qty
                                        else -price_unit*product_uom_qty end) as v_begining,
                                0 as purchase,
                                0 as v_purchase,
                                0 as sales_return,
                                0 as v_sales_return,
                                0 as trans_in,
                                0 as v_trans_in,
                                0 as other_in,
                                0 as v_other_in,
                                0 as sales,
                                0 as v_sales,
                                0 as purc_ret,
                                0 as v_purc_ret,
                                0 as trans_out,
                                0 as v_trans_out,
                                0 as other_out,
                                0 as v_other_out,
                                0 as ending,
                                0 as v_ending,
                                0 as inv_days,
                                0 as pos,
                                0 as v_pos,
                                0 as pos_return,
                                0 as v_pos_return
                        from 
                                stock_move as a
                                left join stock_location as b on a.location_id=b.id
                                left join stock_location as c on a.location_dest_id=c.id
                                left join product_product as d on a.product_id=d.id
                                left join product_template as f on d.product_tmpl_id=f.id
                                left join product_uom as e on f.uom_id=e.id
                                left join product_tire_brand as g on f.pb_tire_brand=g.id
                        where
                                a.state='done'
                                and a.location_dest_id= %s --L3
                                and a.date::date <= %s --D4
                        group by g.name, d.default_code, d.name_template,e.name,d.active
                        order by g.name, d.default_code)

                    UNION ALL
                        -- POS Sales
                        (select
                                g.name as brand,
                                d.default_code as code,
                                d.name_template as product_name,
                                e.name as uom,
                                case 
                                    when d.active='t'
                                        then 'Active'
                                    else 'Non Active' end as status,
                                0 as begining,
                                0 as v_begining,
                                0 as purchase,
                                0 as v_purchase,
                                0 as sales_return,
                                0 as v_sales_return,
                                0 as trans_in,
                                0 as v_trans_in,
                                0 as other_in,
                                0 as v_other_in,
                                0 as sales,
                                0 as v_sales,
                                0 as purc_ret,
                                0 as v_purc_ret,
                                0 as trans_out,
                                0 as v_trans_out,
                                0 as other_out,
                                0 as v_other_out,
                                0 as ending,
                                0 as v_ending,
                                0 as inv_days,    
                                sum(pol.qty) as pos,
                                sum(pol.price_unit*pol.qty) as v_pos,
                                0 as pos_return,
                                0 as v_pos_return
                        from 
                                pos_order_line as pol
                                left join pos_order as po on po.id=pol.order_id
                                left join product_product as d on pol.product_id=d.id
                                left join product_template as f on d.product_tmpl_id=f.id
                                left join product_uom as e on f.uom_id=e.id
                                left join product_tire_brand as g on f.pb_tire_brand=g.id
                        where
                                po.location_id= %s --L11
                                and po.date_order::date between %s and %s --D12,D13
                                and po.state in ('done','paid')
                                and po.is_return is false
                        group by g.name, d.default_code, d.name_template,e.name,d.active
                        order by g.name, d.default_code)

                    UNION ALL
                        -- POS Sales Return
                        (select
                                g.name as brand,
                                d.default_code as code,
                                d.name_template as product_name,
                                e.name as uom,
                                case 
                                    when d.active='t'
                                        then 'Active'
                                    else 'Non Active' end as status,
                                0 as begining,
                                0 as v_begining,
                                0 as purchase,
                                0 as v_purchase,
                                0 as sales_return,
                                0 as v_sales_return,
                                0 as trans_in,
                                0 as v_trans_in,
                                0 as other_in,
                                0 as v_other_in,
                                0 as sales,
                                0 as v_sales,
                                0 as purc_ret,
                                0 as v_purc_ret,
                                0 as trans_out,
                                0 as v_trans_out,
                                0 as other_out,
                                0 as v_other_out,
                                0 as ending,
                                0 as v_ending,
                                0 as inv_days,    
                                0 as pos,
                                0 as v_pos,
                                sum(pol.qty) as pos_return,
                                sum(pol.price_unit*pol.qty) as v_pos_return
                        from 
                                pos_order_line as pol
                                left join pos_order as po on po.id=pol.order_id
                                left join product_product as d on pol.product_id=d.id
                                left join product_template as f on d.product_tmpl_id=f.id
                                left join product_uom as e on f.uom_id=e.id
                                left join product_tire_brand as g on f.pb_tire_brand=g.id
                        where
                                po.location_id= %s --L14
                                and po.date_order::date between %s and %s --D15,D16
                                and po.state in ('cancel','topaid','done')
                                and po.is_return is true
                        group by g.name, d.default_code, d.name_template,e.name,d.active
                        order by g.name, d.default_code)
                    ) AS data
            GROUP BY brand,code,product_name,uom,state
            ORDER BY brand,code,product_name
        """
        self.env.cr.execute(sql,(L,L,L,D2,
                                 L,D1,D2,
                                 L,D1,D2))
        res=self.env.cr.dictfetchall()
        return res
