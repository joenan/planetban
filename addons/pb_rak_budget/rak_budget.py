from openerp.osv import fields, osv

class rak_budget(osv.Model):
    _name = 'rak.budget'

    _columns = {
        'name': fields.char('Name'),
        'shop_id': fields.many2one('pos.shop', 'Shop Name', help='Nama Toko'),
        'analytic_account_id': fields.many2one('account.analytic.account', 'Analytic Account', help='Analytic Account'),
        'rak_class': fields.many2one('rak.budget.class', 'Class'),
        'rak_period': fields.many2one('account.period', 'Period'),
        'rak_total': fields.float('Total'),
        'state': fields.selection([('draft', 'New'),
                                   ('approved', 'Area Approved'),
                                   ('done', 'Region Approved'),
                                   ('cancel', 'Cancel'),
                                   ], 'State'),
        'line_ids': fields.one2many('rak.budget.line', 'rak_id', 'RAK Budget Line'),
    }

    _defaults = {
        'state': 'draft'
    }

    def action_approve_area(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'approved'}, context=context)

    def action_approve_region(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'done'}, context=context)

    def count_total(self, cr, uid, ids, context=None):

        res = {}
        total = 0
        budget_line_obj = self.pool.get('rak.budget.line')
        if ids:
            budget_line_ids = budget_line_obj.search(cr, uid, [('rak_id', '=', ids[0])])
            budget_line = budget_line_obj.browse(cr, uid, budget_line_ids, context=context)

            for line in budget_line:
                total += line.total

        res.update({
            'rak_total': total
        })

        self.write(cr, uid, ids, res, context=context)
        return True

class rak_budget_line(osv.Model):
    _name = 'rak.budget.line'

    _columns = {
        'rak_id': fields.many2one('rak.budget', 'Line ID', ondelete='cascade'),
        'account_id': fields.many2one('account.account', 'Account ID'),
        'total': fields.float('Total'),
    }

class rak_budget_class(osv.Model):
    _name = 'rak.budget.class'

    _columns = {
        'class_id': fields.one2many('rak.budget', 'rak_class', 'Class'),
        'name': fields.char('Class Name'),
        'status': fields.boolean('Status'),
    }