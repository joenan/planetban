{
    'name': '[Planet Ban] RRAK Budget',
    'category': 'Reporting',
    'summary': 'Add RRAK Report Component',
    'version': '1.0',
    'description': """
        RRAK Budget
    """,
    'author': 'Indonesian Odoo Community',
    'depends': ['base', 'point_of_sale', 'account'],
    'update_xml': [
        'rak_budget_view.xml',
    ],
    'data': [],
    'installable': True,
    'active': False,
}
