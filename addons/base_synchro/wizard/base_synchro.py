# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import openerp
from datetime import date
import time
from openerp import pooler
import xmlrpclib
import re
from openerp import tools
import threading
import unicodedata
from openerp.osv import fields, osv
import logging
_logger = logging.getLogger(__name__)

class RPCProxyOne(object):
    def __init__(self, server, ressource):
        self.server = server
        local_url = 'http://%s:%d/xmlrpc/common' % (server.server_url, server.server_port)
        rpc = xmlrpclib.ServerProxy(local_url)
        self.uid = rpc.login(server.server_db, server.login, server.password)
        local_url = 'http://%s:%d/xmlrpc/object' % (server.server_url, server.server_port)
        self.rpc = xmlrpclib.ServerProxy(local_url)
        self.ressource = ressource
    def __getattr__(self, name):
        return lambda cr, uid, *args, **kwargs: self.rpc.execute(self.server.server_db, self.uid, self.server.password, self.ressource, name, *args)

class RPCProxy(object):
    def __init__(self, server):
        self.server = server
    def get(self, ressource):
        return RPCProxyOne(self.server, ressource)

class base_synchro(osv.TransientModel):
    """Base Synchronization """
    _name = 'base.synchro'

    _columns = {
        'server_url': fields.many2one('base.synchro.server', "Server URL", required=True),
        'user_id': fields.many2one('res.users', "Send Result To",),
    }

    _defaults = {
        'user_id': lambda self, cr, uid, context: uid,
    }

    start_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')
    report = []
    report_total = 0
    report_create = 0
    report_write = 0

    def input(self, cr, uid, ids, value, context=None):
        return value
    
    def synchronize(self, cr, uid, server, object, context=None):
        #print "*** Step-3 : synchronize"
        
        #priority for upload first
        #pool = pooler.get_pool(cr.dbname)
        pool = openerp.registry(cr.dbname)
        self.meta = {}
        ids = []
        pool1 = RPCProxy(server)
        pool2 = pool
        if object.action in ('u', 'b'):
            ids += pool2.get('base.synchro.obj').get_ids(cr, uid, object.model_id.model, object.synchronize_date, eval(object.domain),{'action':'u'})
            ids.sort()
            #_logger.error('01.1. [DATA ASAL-OBJ]: %s', object.model_id.model)
            #_logger.error('01.2 [DATA ASAL-RECORD]: %s', ids)
            iii = 0
            for dt, id, action in ids:
                #_logger.error('XXX_IDS: dt: %s - id: %s - action: %s', dt,id,action)
                iii += 1
                pool_src = pool2
                pool_dest = pool1            
                #else:
                #    pool_src = pool2
                #    pool_dest = pool1
                fields = False
                
                #if object.model_id.model == 'crm.case.history':
                #    fields = ['email', 'description', 'log_id']
                    
                value = pool_src.get(object.model_id.model).read(cr, uid, [id], fields, context=context)[0]
                vcreate_date = value['create_date']
                #_logger.error('05. [DATA YANG DI TRANSFER->DATA DILOCAL]: %s', value)
                if 'create_date' in value:
                    del value['create_date']
                if 'write_date' in value:
                    del value['write_date']
                    
                for key , val in value.iteritems():
                    if type(val) == tuple:
                        value.update({key:val[0]})
                value = self.data_transform(cr, uid, pool_src, pool_dest, object.model_id.model, value, action, context=context)   
                #_logger.error('05. [DATA YANG DI TRANSFER->DATA DISERVER]: %s', value)
                id2 = self.get_id(cr, uid, object.id, id, action, context=context)
                if not (iii % 50):
                    pass
                
                # Filter fields to not sync
                for field in object.avoid_ids:
                    if field.name in value:
                        del value[field.name]
                #_logger.error('***************************** NILAI ID2 ******************************')
                #_logger.error('02. [ID TUJUAN]: %s', id2)
                #_logger.error('03. [MODEL JIKA UDAH ADA]: %s', object.model_id.model)
                if id2: #kalau udah ada didalam log maka
                    #_logger.error('***************************** UDAH ADA ******************************')
                    #_logger.error('XXX_synchronize if udah ada id2_22: %s', id2)
                    #print "*** synchronizing [object | local_id] : ", object.model_id.model, id
                    #_logger.error('XXX_synchronize if id2: %s', object.model_id.model)
                    pool_dest.get(object.model_id.model).write(cr, uid, [id2], value, context=context)
                    self.report_total += 1
                    self.report_write += 1
                else:  #kalau belum ada dan isi log nya disini ...
                    #_logger.error('***************************** BLOM ADA ******************************')
                    #_logger.error('VALUE ON SERVER- NAME  - %s', value['name'])
                    #_logger.error('VALUE ON SERVER- CREATE_DATE  - %s', vcreate_date)
                    value_encode = self.input(cr, uid, ids, value, context=context)
                    # --------------------------------------------------------------------------------
                    #jika ada name
                    if value['name']:
                        #if value['price_version_id']: #-- product_pricelist_item
                        if 'code' in value: #-- account_cccount
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('code', '=', value['code'])], context=context)
                        elif 'price_version_id' in value:
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('price_version_id', '=', value['price_version_id'])], context=context)
                        elif 'invoice_id' in value: #-- account_invoice_line
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('invoice_id', '=', value['invoice_id'])], context=context)
                        elif 'location_id' in value: #-- stock_location
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('location_id', '=', value['location_id'])], context=context)
                        elif 'default_location_dest_id' in value: #-- stock_picking_type
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('default_location_dest_id', '=', value['default_location_dest_id'])], context=context)
                        elif 'commercial_partner_id' in value: #-- stock_picking_type
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('commercial_partner_id', '=', value['commercial_partner_id'])], context=context)
                        elif 'currency_id' in value: #-- res_currency_rate
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('currency_id', '=', value['currency_id'])], context=context)
                        elif 'journal_entry_id' in value: #-- account_bank_statement_line
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('journal_entry_id', '=', value['journal_entry_id'])], context=context)
                        elif 'login' in value: #-- res_users
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', ('name', '=', value['name']), ('login', '=', value['login'])], context=context)
                        else:
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date)], context=context)

                    else:
                        if 'login' in value: #res_users
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, [('login', '=', value['login'])], context=context)
                        elif 'name_related' in value: #hr_employee
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', ('name_related', '=', value['name_related']), ('create_date', '>=', vcreate_date)], context=context)
                        elif 'wk_product_template' in value: #product_pack
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('wk_product_template', '=', value['wk_product_template']), ('create_date', '>=', vcreate_date), ('product_name', '=', value['product_name'])], context=context)

                    if svr_data:
                        idnew = svr_data[0]
                    else:
                        idnew = pool_dest.get(object.model_id.model).create(cr, uid, value_encode, context=context)
                    #--------------------------------------------------------------------------------

                    #_logger.error('HASIL JIKA NILAI SAMA  - %s', svr_data)
                    #if idnew:
                    #    _logger.error('NEW DATA CREATE!')
                    #_logger.error('XXX_synchronize blom ada id2_22: %s', value_encode)
                    synid = self.pool.get('base.synchro.obj.line').create(cr, uid, {
                        'obj_id': object.id,
                        'local_id': (action == 'u') and id or idnew,
                        'remote_id': (action == 'd') and id or idnew
                    }, context=context)
                    #if synid:
                    #    _logger.error('LOG CREATED!')
                    local_id = (action == 'u') and id or idnew
                    remote_id = (action == 'd') and id or idnew

                    #_logger.error('ID Log / History : %s', synid) #id base_synchro_obj_line
                    #print "*** synchronized [object | local_id | remote_id] : ", object.model_id.model, local_id, remote_id, " Created"
                    self.report_total += 1
                    self.report_create += 1
                self.meta = {}

        if object.action in ('d','b'):
            ids = pool1.get('base.synchro.obj').get_ids(cr, uid, object.model_id.model, object.synchronize_date, eval(object.domain),{'action':'d'})
            #_logger.error('01.1. [DATA ASAL-OBJ]: %s', object.model_id.model)
            #_logger.error('01.2 [DATA ASAL-RECORD]: %s', ids)
            ids.sort()
            iii = 0
            for dt, id, action in ids:
                iii += 1
                pool_src = pool1
                pool_dest = pool2
                #else:
                #    pool_src = pool2
                #    pool_dest = pool1
                fields = False
                
                #if object.model_id.model == 'crm.case.history':
                #    fields = ['email', 'description', 'log_id']
                    
                value = pool_src.get(object.model_id.model).read(cr, uid, [id], fields, context=context)[0]
                #_logger.error('XX_Download value: %s', value)
                vcreate_date = value['create_date']
                if 'create_date' in value:
                    del value['create_date']
                if 'write_date' in value:
                    del value['write_date']
                for key , val in value.iteritems():
                    if type(val) == tuple:
                        value.update({key:val[0]})
                value = self.data_transform(cr, uid, pool_src, pool_dest, object.model_id.model, value, action, context=context)   
                #_logger.error('05. [DATA YANG DI TRANSFER->DATA DISERVER]: %s', value)
                id2 = self.get_id(cr, uid, object.id, id, action, context=context)
                if not (iii % 50):
                    pass

                # Filter fields to not sync
                for field in object.avoid_ids:
                    if field.name in value:
                        del value[field.name]
                if id2:
                    #print "*** synchronize id2 [object | local_id] : ", object.model_id.model, id
                    pool_dest.get(object.model_id.model).write(cr, uid, [id2], value, context=context)
                    self.report_total += 1
                    self.report_write += 1
                else:
                    #_logger.error('***************************** BLOM ADA ******************************')
                    #_logger.error('VALUE ON SERVER- NAME  - %s', value['name'])
                    #_logger.error('VALUE ON SERVER- CREATE_DATE  - %s', vcreate_date)
                    value_encode = self.input(cr, uid, ids, value, context=context)
                    #jika ada name
                    if value['name']:
                        #if value['price_version_id']: #-- product_pricelist_item
                        if 'code' in value: #-- account_cccount
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('code', '=', value['code'])], context=context)
                        elif 'price_version_id' in value:
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('price_version_id', '=', value['price_version_id'])], context=context)
                        elif 'invoice_id' in value: #-- account_invoice_line
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('invoice_id', '=', value['invoice_id'])], context=context)
                        elif 'location_id' in value: #-- stock_location
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('location_id', '=', value['location_id'])], context=context)
                        elif 'default_location_dest_id' in value: #-- stock_picking_type
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('default_location_dest_id', '=', value['default_location_dest_id'])], context=context)
                        elif 'commercial_partner_id' in value: #-- stock_picking_type
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('commercial_partner_id', '=', value['commercial_partner_id'])], context=context)
                        elif 'currency_id' in value: #-- res_currency_rate
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('currency_id', '=', value['currency_id'])], context=context)
                        elif 'journal_entry_id' in value: #-- account_bank_statement_line
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date), ('journal_entry_id', '=', value['journal_entry_id'])], context=context)
                        elif 'login' in value: #-- account_bank_statement_line
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', ('name', '=', value['name']), ('login', '=', value['login'])], context=context)
                        else:
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', ('name', '=', value['name']), ('create_date', '>=', vcreate_date)], context=context)

                    else:
                        if 'login' in value: #res_users
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, [('login', '=', value['login'])], context=context)
                        elif 'name_related' in value: #hr_employee
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', ('name_related', '=', value['name_related']), ('create_date', '>=', vcreate_date)], context=context)
                        elif 'wk_product_template' in value: #product_pack
                            svr_data = pool_dest.get(object.model_id.model).search(cr, uid, ['&', '&', ('wk_product_template', '=', value['wk_product_template']), ('create_date', '>=', vcreate_date), ('product_name', '=', value['product_name'])], context=context)

                    if svr_data:
                        idnew = svr_data[0]
                    else:
                        idnew = pool_dest.get(object.model_id.model).create(cr, uid, value_encode, context=context)
                    #--------------------------------------------------------------------------------

                    #_logger.error('HASIL JIKA NILAI SAMA  - %s', svr_data)
                    #if idnew:
                        #_logger.error('NEW DATA CREATE!')
                    #idnew = pool_dest.get(object.model_id.model).create(cr, uid, value_encode, context=context)
                    synid = self.pool.get('base.synchro.obj.line').create(cr, uid, {
                        'obj_id': object.id,
                        'local_id': (action == 'u') and id or idnew,
                        'remote_id': (action == 'd') and id or idnew
                    }, context=context)
                    local_id = (action == 'u') and id or idnew
                    remote_id = (action == 'd') and id or idnew
                    #print "*** synchronized [object | local_id | remote_id] : ", object.model_id.model, local_id, remote_id, "  Created"
                    self.report_total += 1
                    self.report_create += 1
                    #_logger.error('XX_Download TABLE CREATE: %s', idnew)
                    #_logger.error('XX_Download LOG: %s', synid)
                self.meta = {}
        return True

    def get_id(self, cr, uid, object_id, id, action, context=None):
        #print "*** Step-6 : get_id (object_id, id, action)",  object_id, id, action       
        #pool = pooler.get_pool(cr.dbname)
        pool = openerp.registry(cr.dbname)
        #print "*** [step-6]  pool, cr.dbname : ", pool, cr.dbname
        line_pool = pool.get('base.synchro.obj.line')
        field_src = (action in ('u','b')) and 'local_id' or 'remote_id'
        field_dest = (action == 'd') and 'local_id' or 'remote_id'
        rid = line_pool.search(cr, uid, [('obj_id', '=', object_id), (field_src, '=', id)], context=context)
        #_logger.error('XXX_History_ID: %s', rid)
        result = False
        if rid:
            result = line_pool.read(cr, uid, rid, [field_dest], context=context)[0][field_dest]
        #_logger.error('XXX_History_RESULT: %s', rid)
        return result

    def relation_transform(self, cr, uid, pool_src, pool_dest, object, id, action, context=None):
        #print "*** Step-5 : relation_transform : "
        
        if not id:
            return False
        #pool = pooler.get_pool(cr.dbname)
        pool = openerp.registry(cr.dbname)
        cr.execute('''select o.id from base_synchro_obj o left join ir_model m on (o.model_id =m.id) where
                m.model=%s and
                o.active''', (object,))
        obj = cr.fetchone()
        result = False
        if obj:
            #
            # If the object is synchronised and found, set it
            #
            result = self.get_id(cr, uid, obj[0], id, action, context=context)
        else:
            #
            # If not synchronized, try to find it with name_get/name_search
            #
            names = pool_src.get(object).name_get(cr, uid, [id])[0][1]
            res = pool_dest.get(object).name_search(cr, uid, names, [], 'like')
            if res:
                result = res[0][0]
            else:
                # LOG this in the report, better message.
                print self.report.append('WARNING: Record "%s" on relation %s not found, set to null.' % (names, object))
        return result

    #applicable for download but not applicable for upload
    def data_transform(self, cr, uid, pool_src, pool_dest, object, data, action='u', context=None):
        #print "*** step-4 : data_transform"       
        
        
        if action == 'u':
            fields = pool_src.get(object).fields_get(cr, uid)
            #_logger.error('XXX_Upload_Data_Transfer_Field_in_Desc: %s', fields)
        else:
            self.meta.setdefault(pool_src, {})
            if not object in self.meta[pool_src]:
                self.meta[pool_src][object] = pool_src.get(object).fields_get(cr, uid)
            fields = self.meta[pool_src][object]        
            #_logger.error('XXX_Else_Data_Transfer_Field_in_Desc: %s', fields)
        for f in fields:
            if f not in data:
                continue
            ftype = fields[f]['type']
            if ftype in ('function', 'one2many', 'one2one'):
                del data[f]
            
            elif ftype == 'many2one':
                if (isinstance(data[f], list)) and data[f]:
                    fdata = data[f][0]
                else:
                    fdata = data[f]
                df = self.relation_transform(cr, uid, pool_src, pool_dest, fields[f]['relation'], fdata, action, context=context)
                data[f] = df
                if not data[f]:
                    del data[f]
            
            elif ftype == 'many2many':
                res = map(lambda x: self.relation_transform(cr, uid, pool_src, pool_dest, fields[f]['relation'], x, action, context=context), data[f])
                data[f] = [(6, 0, [x for x in res if x])]
        del data['id']
        return data

    def upload_download(self, cr, uid, ids, context=None):
        start_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')
        syn_obj = self.browse(cr, uid, ids, context=context)[0]
        
        #pool = pooler.get_pool(cr.dbname)
        pool = openerp.registry(cr.dbname)
        server = pool.get('base.synchro.server').browse(cr, uid, syn_obj.server_url.id, context=context)
        
        for object in server.obj_ids:
            dt = time.strftime('%Y-%m-%d %H:%M:%S')
            #print "*** step-2 : object name : ", object.name 
            self.synchronize(cr, uid, server, object, context=context)
            
            if object.action == 'b':
                time.sleep(1)
                dt = time.strftime('%Y-%m-%d %H:%M:%S')
            self.pool.get('base.synchro.obj').write(cr, uid, [object.id], {'synchronize_date': dt}, context=context)
        end_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')
        
        if syn_obj.user_id:
            #print "[step-2] cr.dbname : ", cr.dbname, syn_obj.user_id, 
            #request = pooler.get_pool(cr.dbname).get('res.users')
            request = openerp.registry(cr.dbname).get('res.users')
            #print "[step-2] request : ", request
            
            if not self.report:
                self.report.append('No exception.')
            summary = '''Here is the synchronization report:

Synchronization started: %s
Synchronization finished: %s

Synchronized records: %d
Records updated: %d
Records created: %d

Exceptions:
            ''' % (start_date, end_date, self.report_total, self.report_write, self.report_create)
            summary += '\n'.join(self.report)
            return True

    def upload_download_multi_thread(self, cr, uid, data, context=None):
        #print "*** Step-1 : upload_download_multi_thread"
        
        #Synchronize process
        threaded_synchronization = threading.Thread(target=self.upload_download, args=(cr, uid, data, context))
        threaded_synchronization.run()
        
        #if successfully synchronized
        data_obj = self.pool.get('ir.model.data')
        id2 = data_obj._get_id(cr, uid, 'base_synchro', 'view_base_synchro_finish')
        if id2:
            id2 = data_obj.browse(cr, uid, id2, context=context).res_id
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'base.synchro',
            'views': [(id2, 'form')],
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
