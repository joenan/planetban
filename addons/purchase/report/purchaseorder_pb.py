
import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class purchaseorder_pb(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(purchaseorder_pb, self).__init__(cr, uid, name, context=context)

        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        partner = user.company_id.partner_id

        self.localcontext.update({
            'time': time,
            'total_qty': self._total_qty,
            'get_supplierinfo': self._get_supplierinfo,
            'get_total_cost': self._get_total_cost,
            'get_date_plus1': self._get_date_plus1
        })

    def _total_qty(self, po_id):
        sql = """
            select sum(product_qty) as total_qty from purchase_order_line
              where order_id=%s
        """ % (po_id)
        self.cr.execute(sql)
        res = self.cr.fetchone()
        return res

    def _get_supplierinfo(self, po_id, product_tmpl_id):
        sql = """
            select coalesce(c.pb_maxprice_excppn,0) as pb_maxprice_excppn,coalesce(c.pb_disc_percent,0) as pb_disc_percent
                from purchase_order_line a
                left outer join product_template b
                on a.product_id=b.id
                left outer join product_supplierinfo c
                on b.id = c.product_tmpl_id
                where a.order_id=%s and a.product_id=%s
                limit 1
        """ % (po_id,product_tmpl_id)
        self.cr.execute(sql)
        res = self.cr.fetchone()
        return res

    def _get_total_cost(self, po_id):
        sql = """
            select sum(product_qty * price_unit) as total_cost
                from purchase_order_line
                where order_id=%s
        """ % (po_id)
        self.cr.execute(sql)
        res = self.cr.fetchone()
        return res


    def _get_product_desc(self, inv_id):
        data={}
        sql = """ select a.description_sale as product_desc
                    from product_template a
                    left outer join product_product d
                    on a.name = d.name_template
                    left outer join account_invoice_line b
                    on d.id = b.product_id
                    left outer join account_invoice c
                    on b.invoice_id = c.id
                    where a.description_sale is not null and c.id=%d"""%(inv_id)
        self.cr.execute(sql)
        data = self.cr.dictfetchall()
        return data

    def _get_date_plus1(self, thedate):
        if not thedate:
            res = {}
        if thedate:
            v_date = datetime.strptime(thedate, "%Y-%m-%d")
            res = (v_date + relativedelta(days=1)).strftime("%d-%m-%Y")
        return res

class report_purchaseorder_pb(osv.AbstractModel):
    _name = 'report.purchase.report_purchaseorder_pb'
    _inherit = 'report.abstract_report'
    _template = 'purchase.report_purchaseorder_pb'
    _wrapped_report_class = purchaseorder_pb
