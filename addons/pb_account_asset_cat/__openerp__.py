{
    'name': '[Planet Ban] Add Asset Category to COA',
    'category': 'Planet Ban',
    'summary': 'Add Asset Category to Chart of Account',
    'version': '1.0',
    'description': """
        Add Asset Category to Chart of Account, and then if create Supplier Invoice Asset Category will be
        automatic fill in to line
    """,
    'author': 'Indonesian Odoo Community',
    'depends': ['base', 'account'],
    'update_xml': [
        'account_account_view.xml',
    ],
    'data': [
        'data/rak_data.xml',
    ],
    'installable': True,
    'active': False,
}
