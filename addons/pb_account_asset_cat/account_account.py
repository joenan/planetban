from openerp.osv import fields, osv

class account_account(osv.Model):

    _name = 'account.account'
    _inherit = 'account.account'

    _columns = {
        'asset_category_id': fields.many2one('account.asset.category', 'Asset Category', help='Asset Category'),
        'cashflow_type': fields.selection([
                                         ('operating', 'Operating'),
                                         ('investing', 'Investing'),
                                         ('finance_activity', 'Financing Activity')],
                                         'Cash Flow'),
        'report_type_id': fields.many2one('account.account.report', 'Report Type', help='Name of Report'),
        'rak_group_id': fields.many2one('account.account.rak', 'RAK Group', help='Rencana dan Realisasi Anggaran Kas (RRAK)'),
    }

class account_account_report(osv.Model):
    _name = 'account.account.report'
    _columns = {
        'account_ids': fields.one2many('account.account', 'report_type_id', 'Account', ondelete='cascade'),
        'name': fields.char('Report Type', size=25),
        'active': fields.boolean('Status'),
    }

    defaults = {
        'active': True,
    }

class account_account_rak(osv.Model):
    _name = 'account.account.rak'
    _columns = {
        'account_ids': fields.one2many('account.account', 'rak_group_id', 'Account', ondelete='cascade'),
        'name': fields.char('RRAK Name', size=25),
        'active': fields.boolean('Status'),
    }

    defaults = {
        'active': True,
    }