# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv
#from openerp import datetime


class query_trial_balance(osv.osv):

    _name = 'account.query_trial_balance'
    _description = 'Query Trial Balance'
    _auto = False

    def function_account_balance(self, cr, uid, ids, fields_name, args, context=None):
        res = {}
        obj_period = self.pool.get('account.period')
        obj_account = self.pool.get('account.account')

        period_id = context.get('period_id', False)
        fiscalyear_id = context.get('fiscalyear_id', False)
        state = context.get('state', 'all')

        period_from = False
        period_to = False


        if period_id:
            #Mencari period opening balance
            kriteria_opening_balance = [('fiscalyear_id','=', fiscalyear_id)]
            period_ids = obj_period.search(cr, uid, kriteria_opening_balance)
            
            for list_index, opening_period_id in enumerate(period_ids):
                if opening_period_id == period_id : previous_period_id = period_ids[list_index-1]
            
            first_period_id = period_ids[0]
            
            beginning_period_from = first_period_id
            beginning_period_to = previous_period_id

            period_from = period_id
            period_to = period_id

            ending_period_from = first_period_id
            ending_period_to = period_id

        for account in self.browse(cr, uid, ids):
            res[account.id] =   {
                                'beginning_debit' : 0.0,
                                'beginning_credit' : 0.0,
                                'debit' : 0.0,
                                'credit' : 0.0,
                                'ending_debit' : 0.0,
                                'ending_credit' : 0.0,
                                }

            if period_id:
                context_beginning = {
                                    'period_from' : beginning_period_from,
                                    'period_to' : beginning_period_to,
                                    }

                context_ending =    {
                                    'period_from' : ending_period_from,
                                    'period_to' : ending_period_to,
                                    }

                context_now =   {
                                'period_from' : period_from,
                                'period_to' : period_to,
                                }

                if state != 'all':
                    context_beginning.update({'state' : state})
                    context_ending.update({'state' : state})
                    context_now.update({'state' : state})


                account_beginning = obj_account.browse(cr, uid, [account.id], context=context_beginning)[0]
                account_ending = obj_account.browse(cr, uid, [account.id], context=context_ending)[0]
                account_now = obj_account.browse(cr, uid, [account.id], context=context_now)[0]

                res[account.id]['beginning_debit'] = account_beginning.debit
                res[account.id]['beginning_credit'] = account_beginning.credit
                res[account.id]['debit'] = account_now.debit
                res[account.id]['credit'] = account_now.credit
                res[account.id]['ending_debit'] = account_ending.debit
                res[account.id]['ending_credit'] = account_ending.credit


        return res

    _columns = {
                'id' : fields.integer(string='ID'),
                'parent_left' : fields.integer(string='Parent Left'),
                'parent_right' : fields.integer(string='Parent Right'),
                'code' : fields.char(string='Code', size=64),
                'currency_id' : fields.many2one(string='Currency', obj='res.currency'),
                'user_type' : fields.many2one(string='User Type', obj='account.account.type'),
                'name' : fields.char(string='Name'),
                'level' : fields.integer(string='Level'),
                'company_id' : fields.many2one(string='Company', obj='res.company'),
                'parent_id' : fields.many2one(string='Parent', obj='account.account'),
                'type' : fields.selection(string='Internal Type', selection=[('view','View'),
                                                                                ('other','Regular'),
                                                                                ('receivable','Receivable'),
                                                                                ('payable','Payable'),
                                                                                ('liquidity','Liquidity'),
                                                                                ('cosolidation', 'Consolidation'),
                                                                                ('closed', 'Closed')]),
                'beginning_debit' : fields.function(string='Beginning Debit', fnct=function_account_balance, type='float', 
                 store=False, method=True, multi='balance'),
                'beginning_credit' : fields.function(string='Beginning Credit', fnct=function_account_balance, type='float', 
                 store=False, method=True, multi='balance'),
                'debit' : fields.function(string='Debit', fnct=function_account_balance, type='float', 
                 store=False, method=True, multi='balance'),
                'credit' : fields.function(string='Credit', fnct=function_account_balance, type='float', 
                 store=False, method=True, multi='balance'),
                'ending_debit' : fields.function(string='Ending Debit', fnct=function_account_balance, type='float', 
                 store=False, method=True, multi='balance'),
                'ending_credit' : fields.function(string='Ending Credit', fnct=function_account_balance, type='float', 
                 store=False, method=True, multi='balance'),
                }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'account_query_trial_balance')
        strSQL =    """
                    CREATE OR REPLACE VIEW account_query_trial_balance AS (
                        SELECT
                            A.id as id,
                            A.parent_left as parent_left,
                            A.parent_right as parent_right,
                            A.code as code,
                            A.currency_id as currency_id,
                            A.user_type as user_type,
                            A.name as name,
                            A.level as level,
                            A.company_id as company_id,
                            A.parent_id as parent_id,
                            A.type as type
                        FROM account_account AS A
                        WHERE   A.active = TRUE AND
                                A.type != 'view'

                    )
                    """
        
        
        cr.execute(strSQL)




query_trial_balance()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
