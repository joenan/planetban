import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_internal_transfer(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_internal_transfer, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_internal_transfer': self._row_internal_transfer,
        })

    def _row_internal_transfer(self, state, src, desc):
        data = {}
        analytic_con = ""
        sql = """
            select
               b.name 	as no_reff,
               b.date	as date,
               c.name	as source_location,
               d.name	as destination_location,
               a.name	as product_name,
               a.product_qty as quantity,
               e.name	as uom
            from stock_move as a
               left join stock_picking as b on a.picking_id=b.id
               left join stock_location as c on b.internal_location_id=c.id
               left join stock_location as d on b.internal_location_dest_id=d.id
               left join product_uom as e on a.product_uom=e.id
            where a.state=%s
               and a.source_trans='internal'
               and b.internal_location_id=%s
               and b.internal_location_dest_id=%s
        """
        self.cr.execute(sql, (state, src, desc))
        data = self.cr.dictfetchall()
        return data

class report_stcok_internal_transfer(osv.AbstractModel):
    _name = 'report.stock.report_internal_transfer'
    _inherit = 'report.abstract_report'
    _template = 'stock.report_internal_transfer'
    _wrapped_report_class = report_internal_transfer


