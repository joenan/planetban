# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv


class query_balance_sheet(osv.osv):

    _name = 'account.query_balance_sheet'
    _description = 'Query Balance Sheet'
    _auto = False

    def function_balance(self, cr, uid, ids, field_name, args, context=None):
        res = {}
        obj_account = self.pool.get('account.account')
        obj_period = self.pool.get('account.period')

        current_period_id = context.get('period_id', False)
        fiscalyear_id = context.get('fiscalyear_id', False)

        state = context.get('state','all')

        criteria = [('fiscalyear_id','=',fiscalyear_id)]

        period_ids = obj_period.search(cr, uid, criteria, order='date_start')
        
        for list_index, period_id in enumerate(period_ids):
            if period_id == current_period_id : previous_period_id = period_ids[list_index-1]

        first_period_id = period_ids[0]
        

        for account in self.browse(cr, uid, ids):
            res[account.id] =   {
                                'previous_period' : 0.0,
                                'current_period' : 0.0,
                                }

            if period_id and fiscalyear_id:
                context_current =   {
                                    'period_from' : first_period_id,
                                    'period_to' : current_period_id,
                                    }

                if state != 'all' : context_current.update({'state' : state})
                account_current = obj_account.browse(cr, uid, [account.id], context=context_current)[0]
                res[account.id]['current_period'] = account_current.balance
               

                context_previous =  {
                                    'period_from' : first_period_id,
                                    'period_to' : previous_period_id,
                                    }

                if state != 'all' : context_previous.update({'state' : state})

                account_previous = obj_account.browse(cr, uid, [account.id], context=context_previous)[0]
                res[account.id]['previous_period'] = account_previous.balance




        return res


    _columns = {
                'id' : fields.integer(string='ID'),
                'parent_left' : fields.integer(string='Parent Left'),
                'parent_right' : fields.integer(string='Parent Right'),
                'code' : fields.char(string='Code', size=64),
                'currency_id' : fields.many2one(string='Currency', obj='res.currency'),
                'user_type' : fields.many2one(string='User Type', obj='account.account.type'),
                'name' : fields.char(string='Name'),
                'level' : fields.integer(string='Level'),
                'company_id' : fields.many2one(string='Company', obj='res.company'),
                'parent_id' : fields.many2one(string='Parent', obj='account.account'),
                'child_ids' : fields.one2many(string='Child', obj='account.account', fields_id='parent_id'),
                'type' : fields.selection(string='Internal Type', selection=[('view','View'),
                                                                                ('other','Regular'),
                                                                                ('receivable','Receivable'),
                                                                                ('payable','Payable'),
                                                                                ('liquidity','Liquidity'),
                                                                                ('cosolidation', 'Consolidation'),
                                                                                ('closed', 'Closed')]),
                'previous_period' : fields.function(string='Previous Period', fnct=function_balance, type='float', method=True, store=False, multi='balance'),
                'current_period' : fields.function(string='Current Period', fnct=function_balance, type='float', method=True, store=False, multi='balance'),

                }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'account_query_balance_sheet')
        strSQL =    """
                    CREATE OR REPLACE VIEW account_query_balance_sheet AS (
                        WITH RECURSIVE account_account_1 AS
                        (
                            SELECT  a.id AS id,
                                    a.parent_left AS parent_left,
                                    a.parent_right AS parent_right,
                                    a.code AS code,
                                    a.currency_id AS currency_id,
                                    a.user_type AS user_type,
                                    a.name AS name,
                                    a.level AS level,
                                    a.company_id AS company_id,
                                    a.parent_id as parent_id,
                                    a.type AS type
                            FROM    account_account AS a

                            UNION ALL
                            SELECT  b.id AS id,
                                    b.parent_left AS parent_left,
                                    b.parent_right AS parent_right,
                                    b.code AS code,
                                    b.currency_id AS currency_id,
                                    b.user_type AS user_type,
                                    b.name AS name,
                                    b.level AS level,
                                    b.company_id AS company_id,
                                    b.parent_id As parent_id,
                                    b.type AS type
                            FROM    account_account b
                            JOIN    account_account_1 AS c ON b.parent_id = c.id

                        )
                        SELECT  d.id as id,
                                d.parent_left as parent_left,
                                d.parent_right as parent_right,
                                d.code as code,
                                d.currency_id as currency_id,
                                d.user_type as user_type,
                                d.name as name,
                                d.level as level,
                                d.company_id as company_id,
                                d.parent_id as parent_id,
                                d.type as type
                        FROM    account_account AS d
                        WHERE   d.parent_id IS NULL
                        UNION ALL
                        SELECT  *
                        FROM    account_account_1

                    )
                    """
        
        
        cr.execute(strSQL)




query_balance_sheet()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
