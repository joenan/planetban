# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv


class query_general_ledger(osv.osv):
    def function_account(self, cr, uid, ids, field_name, args, context=None):
        obj_move = self.pool.get('account.move.line')
        res = {}
        for move in obj_move.browse(cr, uid, ids, context):
            res[move.id] =  {
                            'balance' : move.balance,
                            }
        return res


    _name = 'account.query_general_ledger'
    _description = 'Query General Ledger'
    _auto = False
    _columns = {
                'id' : fields.integer(string='ID'),
                'name' : fields.char(string='Description', size=64),
                'move_id' : fields.many2one(string='# Move', obj='account.move'),
                'account_id' : fields.many2one(string='Account', obj='account.account'),
                'company_id' : fields.many2one(string='Company', obj='res.company'),
                'date' : fields.date(string='Date'),
                'journal_id' : fields.many2one(string='Journal', obj='account.journal'),
                'partner_id' : fields.many2one(string='Partner', obj='res.partner'),
                'period_id' : fields.many2one(string='Period', obj='account.period'),
                'debit' : fields.float(string='Debit'),
                'credit' : fields.float(string='Credit'),
                'balance' : fields.function(string='Balance', fnct=function_account, type='float', method=True, store=False, multi='account'),
                'state' : fields.selection(string='State', selection=[('draft','Unposted'),('posted','Posted')]),
                }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'account_query_general_ledger')
        strSQL =    """
                    CREATE OR REPLACE VIEW account_query_general_ledger AS (
                        SELECT
                                A.id AS id,
                                A.name AS name,
                                A.move_id AS move_id,
                                A.account_id AS account_id,
                                B.company_id AS company_id,
                                B.date as date,
                                B.journal_id AS journal_id,
                                A.partner_id AS partner_id,
                                B.period_id AS period_id,
                                A.debit AS debit,
                                A.credit AS credit,
                                B.state AS state
                        FROM account_move_line AS A
                        JOIN account_move AS B ON A.move_id = B.id
                        JOIN account_journal C ON B.journal_id = C.id
                        JOIN account_account D ON A.account_id = D.id

                    )
                    """
        
        
        cr.execute(strSQL)




query_general_ledger()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
