import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_profit_loss(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_profit_loss, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_profit_loss_by_product': self._row_profit_loss_by_product,
            'row_profit_loss_by_brand': self._row_profit_loss_by_brand,
            'row_profit_loss_by_category': self._row_profit_loss_by_category,
        })

    def _row_profit_loss_by_product(self, date_from, date_to, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " WHERE analytic_account =" + str(analytic_account[0])
        sql = """
            select
                analytic_account
                ,brand
                ,product_id
                --,max(product1) 	as product1
                ,max(product) 	as product
                ,count(product_id)
                ,sum(quantity) 	as  quantity
                ,CASE WHEN sum(quantity) = 0 THEN 0
                 ELSE sum(avg_cogs)/sum(quantity)
                 END  	as avg_cogs
                ,CASE WHEN sum(quantity) = 0 THEN 0
                 ELSE sum(avg_price)/sum(quantity)
                 END 	as avg_price
                ,sum(dpp) 	as dpp
                ,sum(hpp) 	as hpp
                ,sum(percent_hpp) 	as percent_hpp
                ,sum(gross_margin) 	as gross_margin
                ,sum(percent_margin) 	as percent_margin
            from
            (SELECT
                            a.analytic_account_id	as analytic_account
                            , f.name		as brand
                            , a.product_id	as product_id
                            --, a.name		as product1
                            , max(concat(d.default_code,'-',d.name_template)) 	as product
                            , 0			as quantity
                            , sum(a.debit) as avg_cogs
                            , sum(a.credit) as avg_price
                            , sum(a.credit)		as dpp
                            , sum(a.debit)		as hpp
                            , CASE WHEN sum(a.credit)=0 THEN 0
                                ELSE sum(a.debit)/sum(a.credit) * 100
                              END as percent_hpp
                            , sum(a.credit-a.debit)  as gross_margin
                            , CASE WHEN sum(a.credit)=0 THEN 0
                                ELSE abs(sum(a.credit-a.debit)/sum(a.credit) * 100)
                              END as percent_margin
                        FROM 			account_move_line a
                        LEFT OUTER JOIN		account_account b
                          ON 			a.account_id=b.id
                        LEFT OUTER JOIN 	account_move c
                          ON			a.move_id = c.id
                        LEFT OUTER JOIN 	product_product d
                          ON 			a.product_id=d.id
                        LEFT OUTER JOIN 	product_template e
                          ON 			d.product_tmpl_id=e.id
                        LEFT OUTER JOIN 	product_tire_brand f
                          ON 			e.pb_tire_brand=f.id
                        WHERE
                            b.parent_id in (5801,5821)
                        AND 	c.state='posted'
                        AND 	a.date >= %s
                        AND 	a.date <= %s
                        GROUP BY a.analytic_account_id, f.name, a.product_id

                        UNION ALL

            SELECT
               a.account_analytic_id 		as analytic_account
                ,f.name		as brand
                ,a.product_id	as product_id
                --,a.name		as product1
                , max(concat(d.default_code,'-',d.name_template)) 	as product
                ,sum(a.quantity) as quantity
                ,0		as avg_cogs
                ,0		as avg_price
                ,0		as dpp
                ,0		as hpp
                ,0		as percent_hpp
                ,0		as gross_margin
                ,0		as percent_margin
            FROM account_invoice_line a
            LEFT OUTER JOIN account_invoice b
              ON a.invoice_id=b.id
                        LEFT OUTER JOIN 	product_product d
                          ON 			a.product_id=d.id
                        LEFT OUTER JOIN 	product_template e
                          ON 			d.product_tmpl_id=e.id
                        LEFT OUTER JOIN 	product_tire_brand f
                          ON 			e.pb_tire_brand=f.id
            WHERE b.date_invoice >= %s
              AND b.date_invoice <= %s
              AND b.type='out_invoice'
              AND b.state='paid'
            GROUP BY f.name,a.product_id,a.account_analytic_id) as gross_profit
            """ + analytic_con + """
            GROUP BY product_id,brand, analytic_account
            ORDER BY product_id
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to))
        data = self.cr.dictfetchall()
        return data

    def _row_profit_loss_by_brand(self, date_from, date_to, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " WHERE analytic_account =" + str(analytic_account[0])
        sql = """
            select
                analytic_account
                ,brand
                ,sum(quantity) 	as  quantity
                ,CASE WHEN sum(quantity) = 0 THEN 0
                 ELSE sum(avg_cogs)/sum(quantity)
                 END  	as avg_cogs
                ,CASE WHEN sum(quantity) = 0 THEN 0
                 ELSE sum(avg_price)/sum(quantity)
                 END 	as avg_price
                ,sum(dpp) 	as dpp
                ,sum(hpp) 	as hpp
                ,sum(percent_hpp) 	as percent_hpp
                ,sum(gross_margin) 	as gross_margin
                ,sum(percent_margin) 	as percent_margin
            from
            (SELECT
                            a.analytic_account_id	as analytic_account
                            , f.name		as brand
                            , 0			as quantity
                            , sum(a.debit) as avg_cogs
                            , sum(a.credit) as avg_price
                            , sum(a.credit)		as dpp
                            , sum(a.debit)		as hpp
                            , CASE WHEN sum(a.credit)=0 THEN 0
                                ELSE sum(a.debit)/sum(a.credit) * 100
                              END as percent_hpp
                            , sum(a.credit-a.debit)  as gross_margin
                            , CASE WHEN sum(a.credit)=0 THEN 0
                                ELSE abs(sum(a.credit-a.debit)/sum(a.credit) * 100)
                              END as percent_margin
                        FROM 			account_move_line a
                        LEFT OUTER JOIN		account_account b
                          ON 			a.account_id=b.id
                        LEFT OUTER JOIN 	account_move c
                          ON			a.move_id = c.id
                        LEFT OUTER JOIN 	product_product d
                          ON 			a.product_id=d.id
                        LEFT OUTER JOIN 	product_template e
                          ON 			d.product_tmpl_id=e.id
                        LEFT OUTER JOIN 	product_tire_brand f
                          ON 			e.pb_tire_brand=f.id
                        WHERE
                            b.parent_id in (5801,5821)
                        AND 	c.state='posted'
                        AND 	a.date >= %s
                        AND 	a.date <= %s
                        GROUP BY a.analytic_account_id, f.name

                        UNION ALL

            SELECT
                a.account_analytic_id 		as analytic_account
                ,f.name		as brand
                ,sum(a.quantity) as quantity
                ,0		as avg_cogs
                ,0		as avg_price
                ,0		as dpp
                ,0		as hpp
                ,0		as percent_hpp
                ,0		as gross_margin
                ,0		as percent_margin
            FROM account_invoice_line a
            LEFT OUTER JOIN account_invoice b
              ON a.invoice_id=b.id
                        LEFT OUTER JOIN 	product_product d
                          ON 			a.product_id=d.id
                        LEFT OUTER JOIN 	product_template e
                          ON 			d.product_tmpl_id=e.id
                        LEFT OUTER JOIN 	product_tire_brand f
                          ON 			e.pb_tire_brand=f.id
            WHERE b.date_invoice >= %s
              AND b.date_invoice <= %s
              AND b.type='out_invoice'
              AND b.state='paid'
            GROUP BY f.name,a.account_analytic_id) as gross_profit
            """ + analytic_con + """
            GROUP BY brand, analytic_account
            ORDER BY brand
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to))
        data = self.cr.dictfetchall()
        return data

    def _row_profit_loss_by_category(self, date_from, date_to, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " WHERE analytic_account =" + str(analytic_account[0])
        sql = """
            select
                analytic_account
                ,category
                ,sum(quantity) 	as  quantity
                ,CASE WHEN sum(quantity) = 0 THEN 0
                 ELSE sum(avg_cogs)/sum(quantity)
                 END  	as avg_cogs
                ,CASE WHEN sum(quantity) = 0 THEN 0
                 ELSE sum(avg_price)/sum(quantity)
                 END 	as avg_price
                ,sum(dpp) 	as dpp
                ,sum(hpp) 	as hpp
                ,sum(percent_hpp) 	as percent_hpp
                ,sum(gross_margin) 	as gross_margin
                ,sum(percent_margin) 	as percent_margin
            from
            (SELECT
                            a.analytic_account_id	as analytic_account
                            , g.name		as category
                            , 0			as quantity
                            , sum(a.debit) as avg_cogs
                            , sum(a.credit) as avg_price
                            , sum(a.credit)		as dpp
                            , sum(a.debit)		as hpp
                            , CASE WHEN sum(a.credit)=0 THEN 0
                                ELSE sum(a.debit)/sum(a.credit) * 100
                              END as percent_hpp
                            , sum(a.credit-a.debit)  as gross_margin
                            , CASE WHEN sum(a.credit)=0 THEN 0
                                ELSE abs(sum(a.credit-a.debit)/sum(a.credit) * 100)
                              END as percent_margin
                        FROM 			account_move_line a
                        LEFT OUTER JOIN		account_account b
                          ON 			a.account_id=b.id
                        LEFT OUTER JOIN 	account_move c
                          ON			a.move_id = c.id
                        LEFT OUTER JOIN 	product_product d
                          ON 			a.product_id=d.id
                        LEFT OUTER JOIN 	product_template e
                          ON 			d.product_tmpl_id=e.id
                        LEFT OUTER JOIN 	product_category f
                          ON			e.categ_id=f.id
                        LEFT OUTER JOIN 	product_category g
                          ON 			f.parent_id=g.id
                        WHERE
                            b.parent_id in (5801,5821)
                        AND 	c.state='posted'
                        AND 	a.date >= %s
                        AND 	a.date <= %s
                        GROUP BY a.analytic_account_id, g.name

                        UNION ALL

            SELECT
                a.account_analytic_id 		as analytic_account
                ,g.name		as category
                ,sum(a.quantity) as quantity
                ,0		as avg_cogs
                ,0		as avg_price
                ,0		as dpp
                ,0		as hpp
                ,0		as percent_hpp
                ,0		as gross_margin
                ,0		as percent_margin
            FROM account_invoice_line a
            LEFT OUTER JOIN account_invoice b
              ON a.invoice_id=b.id
                        LEFT OUTER JOIN 	product_product d
                          ON 			a.product_id=d.id
                        LEFT OUTER JOIN 	product_template e
                          ON 			d.product_tmpl_id=e.id
                        LEFT OUTER JOIN 	product_category f
                          ON			e.categ_id=f.id
                        LEFT OUTER JOIN 	product_category g
                          ON 			f.parent_id=g.id
            WHERE b.date_invoice >= %s
              AND b.date_invoice <= %s
              AND b.type='out_invoice'
              AND b.state='paid'
            GROUP BY g.name,a.account_analytic_id) as gross_profit
            """ + analytic_con + """
            GROUP BY category, analytic_account
            ORDER BY category
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to))
        data = self.cr.dictfetchall()
        return data


class report_cash_in_out(osv.AbstractModel):
    _name = 'report.account.report_profit_loss'
    _inherit = 'report.abstract_report'
    _template = 'account.report_profit_loss'
    _wrapped_report_class = report_profit_loss


