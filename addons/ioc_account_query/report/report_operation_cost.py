import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_operation_cost(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_operation_cost, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_operation_cost_header': self._row_operation_cost_header,
            'row_operation_cost': self._row_operation_cost,
        })

    def _row_operation_cost_header(self):
        data = {}
        sql = """
            SELECT
                code,name,id
            FROM
                account_account
            WHERE
                code in ('700000','710000','800000','810000') and type='view'
            ORDER BY
                code
        """
        self.cr.execute(sql)
        data = self.cr.dictfetchall()
        return data

    def _where_analytic_account(self, analytic_account):
        data = ""
        if analytic_account:
            data = "and b.analytic_account_id=" + analytic_account
        return data

    def _row_operation_cost(self, date_from, date_to, parent_id, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " and analytic_account_id =" + str(analytic_account[0])
        sql = """
                SELECT
                      a.code                  as code,
                      a.name                  as name,
                      sum(coalesce(amount,0))      as this_month,
                      sum(coalesce(to_month,0))    as to_month
                            FROM
                                    account_account a
                            LEFT JOIN
                            (select analytic_account_id,
                            parent_id,
                            sum(amount)	as amount,
                            sum(to_month)	as to_month
                                    from
                                    (select a.analytic_account_id, b.parent_id, sum(a.debit-a.credit) amount, 0 as to_month
                                    from account_move_line as a
                                    left join account_account as b on a.account_id=b.id
                                    where a.state='valid' and a.date between %s and %s
                                    group by a.analytic_account_id,b.parent_id
                                    UNION ALL

                                    select a.analytic_account_id, b.parent_id, 0, sum(a.debit-a.credit) to_month
                                    from account_move_line as a
                                    left join account_account as b on a.account_id=b.id
                                    where a.state='valid' and a.date<=%s
                                    group by a.analytic_account_id,b.parent_id
                                    ) as A
                                    group by analytic_account_id,parent_id)	as tbl_amount
                                    on a.id=tbl_amount.parent_id

                            WHERE
                                    left(code,1) in ('7','8') and a.type='view' and a.parent_id=%s
                                    --and analytic_account_id=49
                                    """ + analytic_con + """
                            GROUP BY a.code,a.name
                            ORDER BY
                                    a.code,a.name
        """
        self.cr.execute(sql, (date_from, date_to, date_to, parent_id))
        data = self.cr.dictfetchall()
        return data

class report_account_operation_cost(osv.AbstractModel):
    _name = 'report.account.report_operation_cost'
    _inherit = 'report.abstract_report'
    _template = 'account.report_operation_cost'
    _wrapped_report_class = report_operation_cost


