import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_cash_toko(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_cash_toko, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'get_analytic_account_name': self._get_analytic_account_name,
            'row_cash_toko': self._row_cash_toko,
            'row_cash_toko_detail': self._row_cash_toko_detail,
            'get_begining_balance': self._get_begining_balance,

        })

    def _get_analytic_account_name(self, analytic_account_id):
        data = {}
        sql = """
            SELECT
                name
            FROM
                account_analytic_account
            WHERE
                id=%s
            LIMIT 1
        """
        self.cr.execute(sql, (analytic_account_id,))
        data = self.cr.fetchone()
        return data

    def _get_begining_balance(self, analytic_account_id, date_from):
        data = {}
        sql = """
            SELECT
                coalesce(sum(cash-transfer),0)	as beginning_balance
            FROM
            ((select a.analytic_account_id,
                               case when a.journal_id=7 then sum(a.balance_end) end as cash,
                               0 as transfer
                               from account_bank_statement as a
                               where a.state='confirm' and a.balance_end<>0 and a.date <= %s
                               group by a.analytic_account_id,a.date,a.journal_id,company_id
                               order by a.analytic_account_id,a.date,a.journal_id)
                        UNION ALL
                        -----Mencari transaksi dari Kas Besar yang di transfer ke rekening pusat
                        (select analytic_account_id,
                               0 as cash,
                               sum(credit) as transfer
                              from account_move_line where account_id=823 and credit<>0 and date <= %s --4136
                              group by analytic_account_id,date,company_id
                              order by analytic_account_id,date))	AS tbl_beginning
            Where analytic_account_id=%s
        """
        self.cr.execute(sql, (date_from, date_from, analytic_account_id))
        data = self.cr.fetchone()
        return data

    def _row_cash_toko(self, date_from, date_to):
        data = {}
        analytic_con = ""
        sql = """
            ------Mencari amount Sales (inc tax)
            SELECT
                analytic_account_id,
                sum(beginning)	as beginning_balance,
                sum(sales)	as sales,
                sum(cash)	as cash,
                sum(debit)	as debit,
                sum(credit)	as credit,
                sum(cash)+sum(debit) +sum(credit)	as total_pm,
                sum(sales) - (sum(cash) + sum(debit) + sum(credit))	as margin,
                sum(transfer)	as transfer,
                sum(beginning) + sum(sales) - sum(transfer)	as ending_balance,
                company_id
            FROM
            ((select analytic_account_id,
                   date(date_order),
                   sum(amount_total) as beginning,
                   0 as sales,
                   0 as cash,
                   0 as debit,
                   0 as credit,
                   0 as transfer,
                   company_id
                   from pos_order
                  where state='invoiced' and date_order <= %s
                  group by analytic_account_id, date(date_order),company_id
                  order by analytic_account_id, date(date_order))
              UNION ALL
            (select analytic_account_id,
                   date(date_order),
                   0 as beginning,
                   sum(amount_total) as sales,
                   0 as cash,
                   0 as debit,
                   0 as credit,
                   0 as transfer,
                   company_id
                   from pos_order
                  where state='invoiced' and date_order between %s and %s
                  group by analytic_account_id, date(date_order),company_id
                  order by analytic_account_id, date(date_order))
            UNION ALL
            -----Mencari nilai pembayaran berdasarkan payment methode.
            (select a.analytic_account_id,
                   a.date,
                   0 as beginning,
                   0 as sales,
                   coalesce(case when a.journal_id=7 then sum(a.balance_end) end,0) as cash,
                   coalesce(case when a.journal_id=24 then sum(a.balance_end) end,0) as debit,
                   coalesce(case when a.journal_id=25 then sum(a.balance_end) end,0) as credit,
                   0 as transfer,
                   company_id
                   from account_bank_statement as a
                   where a.state='confirm' and a.balance_end<>0 and a.date between %s and %s
                   group by a.analytic_account_id,a.date,a.journal_id,company_id
                   order by a.analytic_account_id,a.date,a.journal_id)


            UNION ALL
            -----Mencari transaksi dari Kas Besar yang di transfer ke rekening pusat
            (select a.analytic_account_id,
                   a.sale_date,
                   0 as beginning,
                   0 as sales,
                   0 as cash,
                   0 as debit,
                   0 as credit,
                   sum(a.credit) as transfer,
                   a.company_id
                  from account_move_line a
                  left join account_move b on a.move_id=b.id
                  where a.account_id=823
                  and a.credit<>0 and a.sale_date between %s AND %s and b.state='posted'-- 4136
                  group by a.analytic_account_id,a.sale_date,a.company_id
                  order by a.analytic_account_id,a.sale_date)) as data
            GROUP BY data.analytic_account_id,company_id
            ORDER BY analytic_account_id
        """
        self.cr.execute(sql, (date_from, date_from, date_to, date_from, date_to, date_from, date_to))
        data = self.cr.dictfetchall()
        return data

    def _row_cash_toko_detail(self, date_from, date_to, analytic_account_id):
        data = {}
        analytic_con = ""
        sql = """
            SELECT
                date,
                coalesce(sum(sales),0)	as sales,
                coalesce(sum(cash),0)	as cash,
                coalesce(sum(debit),0)	as debit,
                coalesce(sum(credit),0)	as credit,
                coalesce(sum(cash)+sum(debit) +sum(credit),0)	as total_pm,
                coalesce(sum(sales) - (sum(cash) + sum(debit) + sum(credit)),0)	as margin,
                coalesce(sum(transfer),0)	as transfer
            FROM
            ((select analytic_account_id,
                   date(date_order),
                   0 as beginning,
                   sum(amount_total) as sales,
                   0 as cash,
                   0 as debit,
                   0 as credit,
                   0 as transfer,
                   company_id
                   from pos_order
                  where state='invoiced' and date_order between %s and %s
                  group by analytic_account_id, date(date_order),company_id
                  order by analytic_account_id, date(date_order))
            UNION ALL
            -----Mencari nilai pembayaran berdasarkan payment methode.
            (select a.analytic_account_id,
                   a.date,
                   0 as beginning,
                   0 as sales,
                   coalesce(case when a.journal_id=7 then sum(a.balance_end) end,0) as cash,
                   coalesce(case when a.journal_id=24 then sum(a.balance_end) end,0) as debit,
                   coalesce(case when a.journal_id=25 then sum(a.balance_end) end,0) as credit,
                   0 as transfer,
                   company_id
                   from account_bank_statement as a
                   where a.state='confirm' and a.balance_end<>0 and a.date between %s and %s
                   group by a.analytic_account_id,a.date,a.journal_id,company_id
                   order by a.analytic_account_id,a.date,a.journal_id)


            UNION ALL
            -----Mencari transaksi dari Kas Besar yang di transfer ke rekening pusat
            (select a.analytic_account_id,
                   a.sale_date,
                   0 as beginning,
                   0 as sales,
                   0 as cash,
                   0 as debit,
                   0 as credit,
                   sum(a.credit) as transfer,
                   a.company_id
                  from account_move_line a
                  left join account_move b on a.move_id=b.id
                  where a.account_id=823 and a.credit<>0
                  and a.sale_date between %s AND %s and b.state='posted' --4136
                  group by a.analytic_account_id,a.sale_date,a.company_id
                  order by a.analytic_account_id,a.sale_date)) as data
            WHERE analytic_account_id=%s
            GROUP BY date
            ORDER BY date
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to, date_from, date_to, analytic_account_id))
        data = self.cr.dictfetchall()
        return data

class report_account_cash_toko(osv.AbstractModel):
    _name = 'report.account.report_cash_toko'
    _inherit = 'report.abstract_report'
    _template = 'account.report_cash_toko'
    _wrapped_report_class = report_cash_toko


