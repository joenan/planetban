import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_profit_loss_all(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_profit_loss_all, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_profit_loss_by_category': self._row_profit_loss_by_category,
            'total_profit_loss_by_category': self._total_profit_loss_by_category,
            'row_profit_loss_by_brand': self._row_profit_loss_by_brand,
            'row_profit_loss_by_product': self._row_profit_loss_by_product,
            'row_profit_loss_by_product_all': self._row_profit_loss_by_product_all,
        })

    def _row_profit_loss_by_category(self, date_from, date_to, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " WHERE analytic_account=" + str(analytic_account[0])
        sql = """
            SELECT
                analytic_account
                ,category
                ,sum(sales_amount) 	as sales_amount
                ,sum(sales_qty) 	as sales_qty
                ,sum(disc_amount) 	as disc_amount
                ,sum(disc_qty) 		as disc_qty
                ,sum(net_amount) 	as net_amount
                ,sum(net_qty) 		as net_qty
                ,sum(cogs) 		as cogs
                ,sum(gross_margin) 	as gross_margin
            FROM
            (SELECT
                a.account_analytic_id				as analytic_account,
                g.name						as category,
                sum(a.quantity*a.price_unit/1.1)		as sales_amount,
                sum(a.quantity)					as sales_qty,
                sum(a.discount/100*a.quantity*a.price_unit/1.1)	as disc_amount,
                sum(case
                       when a.discount<>0 then a.quantity
                       else 0
                       end) 					as disc_qty,
                   sum(a.price_subtotal)				as net_amount,
                   sum(case
                       when a.discount=0 then a.quantity
                       else 0
                       end)						as net_qty,
                   0						as cogs,
                   0						as gross_margin
            FROM
                    account_invoice_line			as a
            LEFT OUTER JOIN	account_invoice 			as b
              on a.invoice_id=b.id
            LEFT OUTER JOIN	account_move				as c
              on b.number=c.name
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            WHERE 		b.type='out_invoice'
              AND 		b.state='paid'
              AND 		b.date_invoice >= %s
              AND 		b.date_invoice <= %s
            GROUP BY  	a.account_analytic_id,g.name

            UNION ALL

            SELECT
                a.analytic_account_id	as analytic_account
                ,g.name			as category
                ,0			as sales_amount
                ,0			as sales_qty
                ,0			as disc_amount
                ,0			as disc_qty
                ,0			as net_amount
                ,0			as net_qty
                , sum(a.debit)		as cogs
                , sum(a.credit-a.debit)  as gross_margin


            FROM 			account_move_line a
            LEFT OUTER JOIN		account_account b
              ON 			a.account_id=b.id
            LEFT OUTER JOIN 	account_move c
              ON			a.move_id = c.id
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id

            WHERE
                b.parent_id in (5801,5821)
                    AND c.state='posted'
                                    AND 	a.date >= %s
                                    AND 	a.date <= %s
            GROUP BY a.analytic_account_id,g.name) as profit_loss
            """ + analytic_con + """
            GROUP BY analytic_account,category
            ORDER BY analytic_account,category
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to))
        data = self.cr.dictfetchall()
        return data

    def _total_profit_loss_by_category(self, date_from, date_to, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " WHERE analytic_account=" + str(analytic_account[0])
        sql = """
            SELECT
                analytic_account
                ,sum(sales_amount) 	as sales_amount
                ,sum(sales_qty) 	as sales_qty
                ,sum(disc_amount) 	as disc_amount
                ,sum(disc_qty) 		as disc_qty
                ,sum(net_amount) 	as net_amount
                ,sum(net_qty) 		as net_qty
                ,sum(cogs) 		as cogs
                ,sum(gross_margin) 	as gross_margin
            FROM
            (SELECT
                a.account_analytic_id				as analytic_account,
                sum(a.quantity*a.price_unit/1.1)		as sales_amount,
                sum(a.quantity)					as sales_qty,
                sum(a.discount/100*a.quantity*a.price_unit/1.1)	as disc_amount,
                sum(case
                       when a.discount<>0 then a.quantity
                       else 0
                       end) 					as disc_qty,
                   sum(a.price_subtotal)				as net_amount,
                   sum(case
                       when a.discount=0 then a.quantity
                       else 0
                       end)						as net_qty,
                   0						as cogs,
                   0						as gross_margin
            FROM
                    account_invoice_line			as a
            LEFT OUTER JOIN	account_invoice 			as b
              on a.invoice_id=b.id
            LEFT OUTER JOIN	account_move				as c
              on b.number=c.name
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            WHERE 		b.type='out_invoice'
              AND 		b.state='paid'
              AND 		b.date_invoice >= %s
              AND 		b.date_invoice <= %s
            GROUP BY  	a.account_analytic_id,g.name

            UNION ALL

            SELECT
                a.analytic_account_id	as analytic_account
                ,0			as sales_amount
                ,0			as sales_qty
                ,0			as disc_amount
                ,0			as disc_qty
                ,0			as net_amount
                ,0			as net_qty
                , sum(a.debit)		as cogs
                , sum(a.credit-a.debit)  as gross_margin


            FROM 			account_move_line a
            LEFT OUTER JOIN		account_account b
              ON 			a.account_id=b.id
            LEFT OUTER JOIN 	account_move c
              ON			a.move_id = c.id
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id

            WHERE
                b.parent_id in (5801,5821)
                    AND c.state='posted'
                                    AND 	a.date >= %s
                                    AND 	a.date <= %s
            GROUP BY a.analytic_account_id,g.name) as profit_loss
            """ + analytic_con + """
            GROUP BY analytic_account

            UNION ALL
            select
                0 			as analytic_account
                ,0 	        as sales_amount
                ,0 	        as sales_qty
                ,0 	        as disc_amount
                ,0 		    as disc_qty
                ,0 	        as net_amount
                ,0 		    as net_qty
                ,0 		    as cogs
                ,0 	        as gross_margin
        LIMIT 1
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to))
        data = self.cr.fetchone()
        return data

    def _row_profit_loss_by_brand(self, date_from, date_to, category, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " AND analytic_account=" + str(analytic_account[0])
        sql = """
            SELECT
                analytic_account
                ,category
                ,brand
                ,sum(sales_amount) 	as sales_amount
                ,sum(sales_qty) 	as sales_qty
                ,sum(disc_amount) 	as disc_amount
                ,sum(disc_qty) 		as disc_qty
                ,sum(net_amount) 	as net_amount
                ,sum(net_qty) 		as net_qty
                ,sum(cogs) 		as cogs
                ,sum(gross_margin) 	as gross_margin
            FROM
            (SELECT
                a.account_analytic_id				as analytic_account,
                g.name						as category,
                h.name						as brand,
                sum(a.quantity*a.price_unit/1.1)		as sales_amount,
                sum(a.quantity)					as sales_qty,
                sum(a.discount/100*a.quantity*a.price_unit/1.1)	as disc_amount,
                sum(case
                       when a.discount<>0 then a.quantity
                       else 0
                       end) 					as disc_qty,
                   sum(a.price_subtotal)				as net_amount,
                   sum(case
                       when a.discount=0 then a.quantity
                       else 0
                       end)						as net_qty,
                   0						as cogs,
                   0						as gross_margin
            FROM
                    account_invoice_line			as a
            LEFT OUTER JOIN	account_invoice 			as b
              on a.invoice_id=b.id
            LEFT OUTER JOIN	account_move				as c
              on b.number=c.name
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            LEFT OUTER JOIN 	product_tire_brand h
              ON 			e.pb_tire_brand=h.id
            WHERE 		b.type='out_invoice'
              AND 		b.state='paid'
              AND 		b.date_invoice >= %s
              AND 		b.date_invoice <= %s
              AND 		h.name is not null
            GROUP BY  	a.account_analytic_id,g.name, h.name

            UNION ALL

            SELECT
                a.analytic_account_id	as analytic_account
                ,g.name			as category
                ,h.name			as brand
                ,0			as sales_amount
                ,0			as sales_qty
                ,0			as disc_amount
                ,0			as disc_qty
                ,0			as net_amount
                ,0			as net_qty
                , sum(a.debit)		as cogs
                , sum(a.credit-a.debit)  as gross_margin


            FROM 			account_move_line a
            LEFT OUTER JOIN		account_account b
              ON 			a.account_id=b.id
            LEFT OUTER JOIN 	account_move c
              ON			a.move_id = c.id
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            LEFT OUTER JOIN 	product_tire_brand h
              ON 			e.pb_tire_brand=h.id

            WHERE
                b.parent_id in (5801,5821)
                    AND c.state='posted'
                                    AND 	a.date >= %s
                                    AND 	a.date <= %s
                                    AND		h.name is not null
            GROUP BY a.analytic_account_id, g.name, h.name) as profit_loss
            WHERE category=%s
            """ + analytic_con + """
            GROUP BY analytic_account,category,brand
            ORDER BY analytic_account,category,brand
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to, category))
        data = self.cr.dictfetchall()
        return data

    def _row_profit_loss_by_product(self, date_from, date_to, category, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " AND analytic_account=" + str(analytic_account[0])
        sql = """
            SELECT
                analytic_account
                ,category
                ,product
                ,sum(sales_amount) 	as sales_amount
                ,sum(sales_qty) 	as sales_qty
                ,sum(disc_amount) 	as disc_amount
                ,sum(disc_qty) 		as disc_qty
                ,sum(net_amount) 	as net_amount
                ,sum(net_qty) 		as net_qty
                ,sum(cogs) 		as cogs
                ,sum(gross_margin) 	as gross_margin
            FROM
            (SELECT
                a.account_analytic_id				as analytic_account,
                g.name						as category,
                e.name						as product,
                sum(a.quantity*a.price_unit/1.1)		as sales_amount,
                sum(a.quantity)					as sales_qty,
                sum(a.discount/100*a.quantity*a.price_unit/1.1)	as disc_amount,
                sum(case
                       when a.discount<>0 then a.quantity
                       else 0
                       end) 					as disc_qty,
                   sum(a.price_subtotal)				as net_amount,
                   sum(case
                       when a.discount=0 then a.quantity
                       else 0
                       end)						as net_qty,
                   0						as cogs,
                   0						as gross_margin
            FROM
                    account_invoice_line			as a
            LEFT OUTER JOIN	account_invoice 			as b
              on a.invoice_id=b.id
            LEFT OUTER JOIN	account_move				as c
              on b.number=c.name
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            LEFT OUTER JOIN 	product_tire_brand h
              ON 			e.pb_tire_brand=h.id
            WHERE 		b.type='out_invoice'
              AND 		b.state='paid'
              AND 		b.date_invoice >= %s
              AND 		b.date_invoice <= %s
              AND 		h.name is null
            GROUP BY  	a.account_analytic_id,g.name, e.name

            UNION ALL

            SELECT
                a.analytic_account_id	as analytic_account
                ,g.name			as category
                ,e.name			as product
                ,0			as sales_amount
                ,0			as sales_qty
                ,0			as disc_amount
                ,0			as disc_qty
                ,0			as net_amount
                ,0			as net_qty
                , sum(a.debit)		as cogs
                , sum(a.credit-a.debit)  as gross_margin


            FROM 			account_move_line a
            LEFT OUTER JOIN		account_account b
              ON 			a.account_id=b.id
            LEFT OUTER JOIN 	account_move c
              ON			a.move_id = c.id
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            LEFT OUTER JOIN 	product_tire_brand h
              ON 			e.pb_tire_brand=h.id

            WHERE
                b.parent_id in (5801,5821)
                    AND c.state='posted'
                                    AND 	a.date >= %s
                                    AND 	a.date <= %s
                                    AND		h.name is null
            GROUP BY a.analytic_account_id,g.name, e.name) as profit_loss
            WHERE category=%s
            """ + analytic_con + """
            GROUP BY analytic_account,category,product
            ORDER BY analytic_account,product,category
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to, category))
        data = self.cr.dictfetchall()
        return data

    def _row_profit_loss_by_product_all(self, date_from, date_to, category, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " AND analytic_account=" + str(analytic_account[0])
        sql = """
            SELECT
                analytic_account
                ,category
                ,product
                ,sum(sales_amount) 	as sales_amount
                ,sum(sales_qty) 	as sales_qty
                ,sum(disc_amount) 	as disc_amount
                ,sum(disc_qty) 		as disc_qty
                ,sum(net_amount) 	as net_amount
                ,sum(net_qty) 		as net_qty
                ,sum(cogs) 		as cogs
                ,sum(gross_margin) 	as gross_margin
            FROM
            (SELECT
            a.account_analytic_id				as analytic_account,
                g.name						as category,
                e.name						as product,
                sum(a.quantity*a.price_unit/1.1)		as sales_amount,
                sum(a.quantity)					as sales_qty,
                sum(a.discount/100*a.quantity*a.price_unit/1.1)	as disc_amount,
                sum(case
                       when a.discount<>0 then a.quantity
                       else 0
                       end) 					as disc_qty,
                   sum(a.price_subtotal)				as net_amount,
                   sum(case
                       when a.discount=0 then a.quantity
                       else 0
                       end)						as net_qty,
                   0						as cogs,
                   0						as gross_margin
            FROM
                    account_invoice_line			as a
            LEFT OUTER JOIN	account_invoice 			as b
              on a.invoice_id=b.id
            LEFT OUTER JOIN	account_move				as c
              on b.number=c.name
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            LEFT OUTER JOIN 	product_tire_brand h
              ON 			e.pb_tire_brand=h.id
            WHERE 		b.type='out_invoice'
              AND 		b.state='paid'
              AND 		b.date_invoice >= %s
              AND 		b.date_invoice <= %s
            GROUP BY  	a.account_analytic_id,g.name, e.name

            UNION ALL

            SELECT
            a.analytic_account_id 	as analytic_account
                ,g.name			as category
                ,e.name			as product
                ,0			as sales_amount
                ,0			as sales_qty
                ,0			as disc_amount
                ,0			as disc_qty
                ,0			as net_amount
                ,0			as net_qty
                , sum(a.debit)		as cogs
                , sum(a.credit-a.debit)  as gross_margin


            FROM 			account_move_line a
            LEFT OUTER JOIN		account_account b
              ON 			a.account_id=b.id
            LEFT OUTER JOIN 	account_move c
              ON			a.move_id = c.id
            LEFT OUTER JOIN 	product_product d
              ON 			a.product_id=d.id
            LEFT OUTER JOIN 	product_template e
              ON 			d.product_tmpl_id=e.id
            LEFT OUTER JOIN 	product_category f
              ON			e.categ_id=f.id
            LEFT OUTER JOIN 	product_category g
              ON 			f.parent_id=g.id
            LEFT OUTER JOIN 	product_tire_brand h
              ON 			e.pb_tire_brand=h.id

            WHERE
                b.parent_id in (5801,5821)
                    AND c.state='posted'
                                    AND 	a.date >= %s
                                    AND 	a.date <= %s
            GROUP BY a.analytic_account_id,g.name, e.name) as profit_loss
        WHERE category=%s
        """ + analytic_con + """
        GROUP BY analytic_account,category,product
        ORDER BY analytic_account,product,category
        """
        self.cr.execute(sql, (date_from, date_to, date_from, date_to, category))
        data = self.cr.dictfetchall()
        return data


class report_cash_in_out_all(osv.AbstractModel):
    _name = 'report.account.report_profit_loss_all'
    _inherit = 'report.abstract_report'
    _template = 'account.report_profit_loss_all'
    _wrapped_report_class = report_profit_loss_all


