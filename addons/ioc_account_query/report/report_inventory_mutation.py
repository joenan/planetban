import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta
import logging
_logger = logging.getLogger(__name__)

class report_inventory_mutation(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_inventory_mutation, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_product_mutation': self._row_product_mutation
        })

    def _get_product_info(self, product_id):
        sql = """

        """
        self.cr.execute(sql)
        res = self.cr.fetchone()
        return res

    def _row_product_mutation(self, location_id, date_from, date_to):
        data = {}
        sql = """
            SELECT
                brand,
                code,
                product_name,
                uom,
                status 			AS state,
                sum(v_begining)	as begining_qty,
                sum(v_purchase)		as purchase,
                sum(v_sales_return)	as sales_return,
                sum(v_trans_in)	as transfer_in,
                sum(v_other_in)		as others_in,
                sum(v_sales)		as sales,
                sum(v_purc_ret)	as purchase_return,
                sum(v_trans_out)	as transfer_out,
                sum(v_other_out)		as others_out,
                sum(v_ending)	as ending_balance,
                sum(begining)	as qty_begining_qty,
                sum(purchase)	as qty_purchase,
                sum(sales_return)	as qty_sales_return,
                sum(trans_in)	as qty_transfer_in,
                sum(other_in)	as qty_others_in,
                sum(sales)		as qty_sales,
                sum(purc_ret)	as qty_purchase_return,
                sum(trans_out)	as qty_transfer_out,
                sum(other_out)	as qty_others_out,
                sum(ending)	as qty_ending_balance

            FROM
                            (
                ------------------------
                --- Begining Balance
                ------------------------
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    sum(case when (c.usage='internal' and a.location_dest_id=%s) then product_uom_qty else - product_uom_qty end) as begining,
                    sum(case when (c.usage='internal' and a.location_dest_id=%s) then price_unit*product_uom_qty else -price_unit*product_uom_qty end) as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_id=%s or a.location_dest_id=%s)
                      and a.date::date <= %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)

                ------------------------
                --- Purchase
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    sum(a.product_uom_qty)	as purchase,
                    sum(a.price_unit*a.product_uom_qty)	as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_dest_id=%s and b.usage='supplier')
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Sales Return
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    sum(a.product_uom_qty)	as sales_return,
                    sum(a.product_uom_qty*a.price_unit)	as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_dest_id=%s and b.usage='customer')
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Transfer In
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    sum(a.product_uom_qty)	as trans_in,
                    sum(a.product_uom_qty*a.price_unit)	as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_dest_id=%s and b.usage='internal')
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Other In
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    sum(a.product_uom_qty)	as other_in,
                    sum(a.product_uom_qty*price_unit)	as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_dest_id=%s and b.usage not in ('supplier','customer','internal'))
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Sales
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    sum(a.product_uom_qty)	as sales,
                    sum(a.product_uom_qty*a.price_unit)	as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_id=%s and c.usage='customer')
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Purchase Return
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    sum(a.product_uom_qty)	as purc_ret,
                    sum(a.product_uom_qty*a.price_unit)	as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_id=%s and c.usage='supplier')
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Transfer Out
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    sum(a.product_uom_qty)	as trans_out,
                    sum(a.product_uom_qty*a.price_unit)	as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_id=%s and c.usage='internal')
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)

                ------------------------
                --- Other Out
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    sum(a.product_uom_qty)	as other_out,
                    sum(a.product_uom_qty*a.price_unit)	as v_other_out,
                    0			as ending,
                    0			as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_id=%s and c.usage not in ('customer','supplier','internal'))
                      and a.date::date between %s and %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)


                ------------------------
                --- Ending Balance
                ------------------------
                UNION ALL
                (select 	g.name			as brand,
                    d.default_code		as code,
                    d.name_template		as product_name,
                    e.name			as uom,
                    case when d.active='t' then 'Active' else 'Non Active' end as status,
                    0			as begining,
                    0			as v_begining,
                    0			as purchase,
                    0			as v_purchase,
                    0			as sales_return,
                    0			as v_sales_return,
                    0			as trans_in,
                    0			as v_trans_in,
                    0			as other_in,
                    0			as v_other_in,
                    0			as sales,
                    0			as v_sales,
                    0			as purc_ret,
                    0			as v_purc_ret,
                    0			as trans_out,
                    0			as v_trans_out,
                    0			as other_out,
                    0			as v_other_out,
                    sum(case when (c.usage='internal' and a.location_dest_id=%s) then product_uom_qty else - product_uom_qty end) as ending,
                    sum(case when (c.usage='internal' and a.location_dest_id=%s) then price_unit*product_uom_qty else -price_unit*product_uom_qty end) as v_ending,
                    0			as inv_days
                from stock_move 		as a
                     left join stock_location 	as b on a.location_id=b.id
                     left join stock_location 	as c on a.location_dest_id=c.id
                     left join product_product 	as d on a.product_id=d.id
                     -- left join product_uom	as e on a.product_uom=e.id
                     left join product_template	as f on d.product_tmpl_id=f.id
                     left join product_uom	as e on f.uom_id=e.id
                     left join product_tire_brand as g on f.pb_tire_brand=g.id
                where a.state='done'
                      and (a.location_id=%s or a.location_dest_id=%s)
                      and a.date::date <= %s
                group by g.name, d.default_code, d.name_template,e.name,d.active
                order by g.name, d.default_code)

                ) AS data
                            GROUP BY
                                brand,
                                code,
                                product_name,
                                uom,
                                state
                            ORDER BY
                                brand,
                                code,
                                product_name
        """
        # Begining Balance
        #   L, L, L, L, S
        # Purchase
        #   L, S, E
        # Sales Return
        #   L, S, E
        # Transfer In
        #   L, S, E
        # Others In
        #   L, S, E
        # Sales
        #   L, S, E
        # Sales Return
        #   L, S, E
        # Transfer Out
        #   L, S, E
        # Others Out
        #   L, S, E
        # Ending Balance
        #   L, L, L, L, E

        self.cr.execute(sql, (location_id, location_id, location_id, location_id, date_from,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, date_from, date_to,
                              location_id, location_id, location_id, location_id, date_to,

                            ))
        data = self.cr.dictfetchall()
        _logger.error('_DHA: %s', sql)
        return data

    def _beginning_balance(self, product_id, date_from, company_id, location_id):
        sql = """

        """
        self.cr.execute(sql, (date_from, product_id, company_id, location_id))
        res = self.cr.fetchone()
        return res

    def _get_company_info(self, company_id):
        sql = """

        """
        self.cr.execute(sql, (company_id))
        res = self.cr.fetchone()
        return res


class report_product_inventory_mutation(osv.AbstractModel):
    _name = 'report.product.report_inventory_mutation'
    _inherit = 'report.abstract_report'
    _template = 'product.report_inventory_mutation'
    _wrapped_report_class = report_inventory_mutation

class report_product_inventory_mutation_noprice(osv.AbstractModel):
    _name = 'report.product.report_inventory_mutation_noprice'
    _inherit = 'report.abstract_report'
    _template = 'product.report_inventory_mutation_noprice'
    _wrapped_report_class = report_inventory_mutation


