import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class cash_in_out(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(cash_in_out, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_cash_in_out':self._row_cash_in_out,
            'get_beginning_balance': self._get_beginning_balance,
            'get_coa_name': self._get_coa_name,
        })

    def _get_coa_name(self, coa_id):
        data = {}
        if not coa_id:
            coa_id = "id = 0"
        else:
            if len(coa_id) == 1:
                coa_id = "id = " + str(coa_id[0])
            else:
                coa_id = "id in " + str(tuple(coa_id))
        sql = """
        SELECT
            id,code,name
        FROM
            account_account
        WHERE
            """ + coa_id + """
        ORDER by code
        """
        self.cr.execute(sql)
        data = self.cr.dictfetchall()
        return data

    def _get_beginning_balance(self, date_from, state, coa, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " AND a.analytic_account_id =" + str(analytic_account[0])
        sql = """
        SELECT
            coalesce(sum(A.debit),0.00) 	AS debit,
            coalesce(sum(A.credit),0.00) 	AS credit,
            coalesce(sum(A.debit) - sum(A.credit),0.00) as balance
        FROM    account_move_line AS A
            LEFT OUTER JOIN    account_move AS B ON A.move_id = B.id
            LEFT OUTER JOIN 	account_analytic_account as C ON a.analytic_account_id=c.id
            LEFT OUTER JOIN    account_account D ON A.account_id = D.id
            LEFT OUTER JOIN    res_partner E ON A.partner_id = E.id
            LEFT OUTER JOIN    account_period F ON B.period_id = F.id
        WHERE
            a.COMPANY_ID=1
            AND a.date < %s
            AND b.state= %s
            AND d.id = %s
            """ + analytic_con + """

        """
        self.cr.execute(sql, (date_from, state, coa))
        data = self.cr.fetchone()
        return data



    def _row_cash_in_out(self, date_from, date_to, state, coa, analytic_account):
        data = {}
        analytic_con = ""
        if analytic_account:
            analytic_con = " AND a.analytic_account_id =" + str(analytic_account[0])
        sql = """
        SELECT
            A.id 		AS id,
            A.name 		AS name,
            A.move_id 	AS move_id,
            B.name 		AS move_name,
            A.account_id 	AS account_id,
            D.name		AS coa,
            B.company_id 	AS company_id,
            B.date 		AS date,
            B.journal_id 	AS journal_id,
            G.name		AS journal_name,
            A.partner_id 	AS partner_id,
            E.name 		AS partner_name,
            B.period_id 	AS period_id,
            F.name		AS period_name,
            A.debit 	AS debit,
            A.credit 	AS credit,
            B.state 	AS state,
            A.currency_id 	AS currency_id,
            CASE
                WHEN A.amount_currency > 0 THEN A.amount_currency
            ELSE 0
            END AS debit_amount_currency,
            CASE
                WHEN A.amount_currency < 0 THEN ABS(A.amount_currency)
            ELSE 0
            END AS credit_amount_currency,
            D.user_type
        FROM    account_move_line AS A
            LEFT OUTER JOIN    account_move AS B ON A.move_id = B.id
            LEFT OUTER JOIN 	account_analytic_account as C ON a.analytic_account_id=c.id
            LEFT OUTER JOIN    account_account D ON A.account_id = D.id
            LEFT OUTER JOIN    res_partner E ON A.partner_id = E.id
            LEFT OUTER JOIN    account_period F ON B.period_id = F.id
            LEFT OUTER JOIN    account_journal G ON a.journal_id = G.id
        WHERE
            a.COMPANY_ID=1 AND
            a.date >= %s AND
            a.date <= %s AND
            b.state= %s AND
            D.id = %s
            """ + analytic_con + """
        ORDER BY    A.date,A.id
        """
        self.cr.execute(sql, (date_from, date_to, state, coa))
        data = self.cr.dictfetchall()
        return data


class report_cash_in_out(osv.AbstractModel):
    _name = 'report.account.report_cash_in_out'
    _inherit = 'report.abstract_report'
    _template = 'account.report_cash_in_out'
    _wrapped_report_class = cash_in_out
