import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_outstanding_po(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_outstanding_po, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_outstanding_po': self._row_outstanding_po,
        })

    def _row_outstanding_po(self, date_from, date_to):
        data = {}
        analytic_con = ""
        sql = """
            select
               a.origin 	as po_no,
               d.date_order as po_date,
               b.name 	as supplier_name,
               c.name 	as picking_no,
               a.name 	as product_name,
               a.product_qty as qty_OutStd,
               f.name	as uom
            from stock_move as a
            left join stock_picking as c on a.picking_id=c.id
            left join res_partner as b on c.partner_id=b.id
            left join purchase_order as d on a.origin=d.name
            left join product_template as e on a.product_id=e.id
            left join product_uom as f on a.product_uom=f.id
            where
               a.state='assigned'    ---- 'Available'
               and e.type='product'  ---- 'Khusus untuk product stockable'
               and d.date_order::date between %s and %s
            order by b.name,a.origin
        """
        self.cr.execute(sql, (date_from, date_to))
        data = self.cr.dictfetchall()
        return data

class report_stcok_outstanding_po(osv.AbstractModel):
    _name = 'report.stock.report_outstanding_po'
    _inherit = 'report.abstract_report'
    _template = 'stock.report_outstanding_po'
    _wrapped_report_class = report_outstanding_po


