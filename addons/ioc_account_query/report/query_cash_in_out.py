# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv
# from lxml import etree


class query_cash_in_out(osv.osv):

    _name = 'account.query_cash_in_out'
    _description = 'Query Cash In/Out'
    _auto = False
    _columns = {
                'name' : fields.char(string='Description', size=64),
                'move_id' : fields.many2one(string='# Move', obj='account.move'),
                'account_id' : fields.many2one(string='Account', obj='account.account'),
                'company_id' : fields.many2one(string='Company', obj='res.company'),
                'date' : fields.date(string='Date'),
                'journal_id' : fields.many2one(string='Journal', obj='account.journal'),
                'partner_id' : fields.many2one(string='Partner', obj='res.partner'),
                'period_id' : fields.many2one(string='Period', obj='account.period'),
                'debit' : fields.float(string='In'),
                'credit' : fields.float(string='Out'),
                'currency_id' : fields.many2one(string='Secondary Currency', obj='res.currency'),
                'debit_amount_currency' : fields.float(string='In'),
                'credit_amount_currency' : fields.float(string='Out'),
                'state' : fields.selection(string='State', selection=[('draft','Unposted'),('posted','Posted')]),
                }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'account_query_cash_in_out')
        strSQL =    """
                    CREATE OR REPLACE VIEW account_query_cash_in_out AS (
                        SELECT
                                A.id AS id,
                                A.name AS name,
                                A.move_id AS move_id,
                                A.account_id AS account_id,
                                B.company_id AS company_id,
                                B.date as date,
                                B.journal_id AS journal_id,
                                A.partner_id AS partner_id,
                                B.period_id AS period_id,
                                A.debit AS debit,
                                A.credit AS credit,
                                B.state AS state,
                                A.currency_id AS currency_id,
                                CASE
                                    WHEN A.amount_currency > 0 THEN A.amount_currency
                                    ELSE 0
                                END AS debit_amount_currency,
                                CASE
                                    WHEN A.amount_currency < 0 THEN ABS(A.amount_currency)
                                    ELSE 0
                                END AS credit_amount_currency
                                       
                        FROM    account_move_line AS A
                        JOIN    account_move AS B ON A.move_id = B.id
                        JOIN    account_journal C ON B.journal_id = C.id
                        JOIN    account_account D ON A.account_id = D.id
                        JOIN    (
                                SELECT  res_id,
                                        module,
                                        model,
                                        name
                                FROM    ir_model_data
                                WHERE   module = 'l10n_id_account_type' AND
                                        model = 'account.account.type' AND
                                        name = 'accountType_kas'
                                ) AS E ON D.user_type = E.res_id
                        WHERE   (C.type = 'cash') 
                    )
                    """
        
        
        cr.execute(strSQL)


query_cash_in_out()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
