# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv
from lxml import etree
from datetime import datetime


class query_receivable_aging(osv.osv):

    _name = 'account.query_receivable_aging'
    _description = 'Query Receivable Aging'
    _auto = False

    def function_aging(self, cr, uid, ids, fields_name, args, context=None):
        res = {}
        obj_move_line = self.pool.get('account.move.line')

        period_lenght = context.get('period_lenght',False)
        date = context.get('date', datetime.now().strftime('%Y-%m-%d'))

        if date:
            ord_date = datetime.strptime(date, '%Y-%m-%d').toordinal()

        if period_lenght:
            total_period = period_lenght * 5


        for line in self.browse(cr, uid, ids):
            res[line.id] =  {
                            'aging1' : 0.0,
                            'aging2' : 0.0,
                            'aging3' : 0.0,
                            'aging4' : 0.0,
                            'aging5' : 0.0,
                            'amount_residual' : 0.0,
                            'amount_residual_currency' : 0.0,
                            }


            if line.date_due:
                ord_date_due = datetime.strptime(line.date_due, '%Y-%m-%d').toordinal()

                a = abs(ord_date_due - ord_date)
                move_line = obj_move_line.browse(cr, uid, [line.id], context={'date' : date})[0]

                res[line.id]['amount_residual'] = move_line.amount_residual
                res[line.id]['amount_residual_currency'] = move_line.amount_residual_currency


                for interval in range(1, 6):
                    if (period_lenght * (interval - 1)) <= a < (period_lenght * (interval)):
                        res[line.id]['aging%s' % (interval)] = move_line.amount_residual

                    if interval == 5:
                        if a >= (period_lenght * 4):
                            res[line.id]['aging5'] = move_line.amount_residual
                        
        return res

    _columns = {
                'id' : fields.integer(string='ID'),
                'name' : fields.char(string='Description', size=64),
                'move_id' : fields.many2one(string='# Move', obj='account.move'),
                'account_id' : fields.many2one(string='Account', obj='account.account'),
                'company_id' : fields.many2one(string='Company', obj='res.company'),
                'date' : fields.date(string='Date'),
                'date_due' : fields.date(string='Date Due'),
                'journal_id' : fields.many2one(string='Journal', obj='account.journal'),
                'partner_id' : fields.many2one(string='Partner', obj='res.partner'),
                'period_id' : fields.many2one(string='Period', obj='account.period'),
                'respective_currency_id' : fields.many2one(string='Respective Currency', obj='res.currency'),
                'base_currency_id' : fields.many2one(string='Base Currency', obj='res.currency'),
                'reconcile_id' : fields.many2one(string='Reconcile', obj='account.move.reconcile'),
                'reconcile_partial_id' : fields.many2one(string='Partial Reconcile', obj='account.move.reconcile'),
                'debit' : fields.float(string='Debit'),
                'credit' : fields.float(string='Credit'),
                'amount_currency' : fields.float(string='Amount Currency'),
                'amount_residual' : fields.function(string='Amount Residual', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'amount_residual_currency' : fields.function(string='Aging1', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'aging1' : fields.function(string='Aging1', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'aging2' : fields.function(string='Aging2', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'aging3' : fields.function(string='Aging3', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'aging4' : fields.function(string='Aging4', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'aging5' : fields.function(string='Aging5', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
                'state' : fields.selection(string='State', selection=[('draft','Unposted'),('posted','Posted')]),
                }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'account_query_receivable_aging')
        strSQL =    """
                    CREATE OR REPLACE VIEW account_query_receivable_aging AS (
                        SELECT
                                A.id AS id,
                                A.name AS name,
                                A.move_id AS move_id,
                                A.account_id AS account_id,
                                B.company_id AS company_id,
                                B.date as date,
                                A.date_maturity as date_due,
                                B.journal_id AS journal_id,
                                A.partner_id AS partner_id,
                                B.period_id AS period_id,
                                CASE
                                    WHEN A.currency_id IS NULL THEN E.currency_id
                                    ELSE A.currency_id 
                                END AS respective_currency_id,
                                E.currency_id AS base_currency_id,
                                A.reconcile_id AS reconcile_id,
                                A.reconcile_partial_id AS reconcile_partial_id,
                                A.debit AS debit,
                                A.credit AS credit,
                                A.amount_currency AS amount_currency,
                                B.state AS state
                        FROM account_move_line AS A
                        JOIN account_move AS B ON A.move_id = B.id
                        JOIN account_journal C ON B.journal_id = C.id
                        JOIN account_account D ON A.account_id = D.id
                        JOIN res_company E ON B.company_id = E.id
                        WHERE   (C.type = 'sale') AND
                                (D.type = 'receivable')
                    )
                    """
        
        
        cr.execute(strSQL)

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        obj_user = self.pool.get('res.users')
        obj_journal = self.pool.get('account.journal')
        obj_year = self.pool.get('account.fiscalyear')

        user = obj_user.browse(cr, uid, [uid])[0]


        res = super(query_receivable_aging, self).fields_view_get(cr, uid, view_id, view_type, context, toolbar, submenu)

        if view_type == 'tree':
            period_lenght = context.get('period_lenght', False)
            aging_label = {}

            if period_lenght:
                total_lenght = period_lenght * 5

                doc = etree.XML(res['arch'])

                for interval in range(1,6):
                    aging_label['aging%s' % interval] = '%s - %s' % (str(period_lenght * (interval-1)), str(period_lenght * (interval)))

                    if interval == 5:
                        aging_label['aging5'] = '+%s' % (str(period_lenght * (4)))

                    for node in doc.xpath("//field[@name='aging%s']" % interval):
                        node.set('string',aging_label['aging%s' % interval])

                res['arch'] = etree.tostring(doc)

        return res




query_receivable_aging()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
