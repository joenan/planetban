# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv
# from lxml import etree


class query_order_analyst_payment(osv.osv):

    _name = 'pos.query_order_analyst_payment'
    _description = 'BI Order Analyst per payment method'
    _auto = False
    _order = 'date,id'
    _columns = {
        'id': fields.integer('ID'),
        'analytic_account' : fields.char(string='Analytic Account', size=64),
        'date': fields.char(string='Date', size=64),
        'year': fields.char(string='Year', size=4),
        'month': fields.char(string='Month', size=2),
        'day': fields.char(string='Day', size=2),
        'pos_reference': fields.char(string='POS Ref', size=64),
        'order_reference': fields.char(string='Order Ref', size=64),
        'customer': fields.char(string='Customer', size=64),
        'amount': fields.float(string='Amount'),
        'payment': fields.char(string='Payment Type', size=64),
        'stock_location_id': fields.many2one('stock.location', string='Location')

    }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'pos_query_order_analyst_payment')
        strSQL = """
                    CREATE OR REPLACE VIEW pos_query_order_analyst_payment AS (
                        select
                           a.id as id,
                           e.name	analytic_account,
                           a.date,
                           date_part('year',a.date) as year,
                           date_part('month',a.date) as month,
                           date_part('day',a.date) as day,
                           a.ref  	pos_reference,
                           a.name	order_reference,
                           c.name	customer,
                           a.amount,
                           b.name	payment,
                           h.stock_location_id

                        from
                           account_bank_statement_line as a
                           left join account_journal as b	   on a.journal_id=b.id
                           left join res_partner as c		   on a.partner_id=c.id
                           left join account_bank_statement as d   on a.statement_id=d.id
                           left join account_analytic_account as e on d.analytic_account_id=e.id
                           left join pos_order as f		   on b.id=f.sale_journal
                           left join pos_session as g		   on d.pos_session_id=g.id
                           left join pos_config as h		   on g.config_id=h.id
                        where
                           a.amount<>0
                        order by
                           a.date)
                    """
        cr.execute(strSQL)

#query_order_analyst_payment()
