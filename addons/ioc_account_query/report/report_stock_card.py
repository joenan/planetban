import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class report_stock_card(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_stock_card, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'get_product_info': self._get_product_info,
            'row_stock_card': self._row_stock_card,
            'beginning_balance': self._beginning_balance,
        })

    def _get_product_info(self, product_id):
        sql = """
            select
                a.default_code 		as code
                , a.name_template 	as product_name
                , c.name 		as uom
            from product_product a
            left outer join product_template b
            on a.product_tmpl_id=b.id
            left outer join product_uom c
            on b.uom_id=c.id
            where a.id=""" + str(product_id) + """
        """
        self.cr.execute(sql)
        res = self.cr.fetchone()
        return res

    def _row_stock_card(self, product_id, date_from, date_to, company_id, location_id):
        data = {}
        sql = """
            select b.complete_name,
                   c.complete_name,
                   h.name		as brand,
                   d.default_code,
                   d.name_template,
                   e.name		as uom,
                   date(a.date)	as trans_date,
                   f.name		as picking,
                   a.origin     as description,
                   case when a.location_dest_id=%s then coalesce(a.product_uom_qty,0)
                        else 0
                   end     stock_in,
                   case when a.location_id=%s then coalesce(a.product_uom_qty,0)
                        else 0
                   end     stock_out,
                   coalesce(a.price_unit,0)   as price_unit_on_quant,
                   case when a.location_dest_id=%s then coalesce(a.product_uom_qty*a.price_unit,0)
                        else 0
                   end     value_in,
                   case when a.location_id=%s then coalesce(a.product_uom_qty*a.price_unit,0)
                        else 0
                   end     value_out
            from stock_move 		as a
            left join stock_location     	as b on a.location_id=b.id
            left join stock_location	as c on a.location_dest_id=c.id
            left join product_product	as d on a.product_id=d.id
            left join product_uom		as e on a.product_uom=e.id
            left join stock_picking		as f on a.picking_id=f.id
            left join product_template	as g on d.product_tmpl_id=g.id
            left join product_tire_brand	as h on g.pb_tire_brand=h.id
            where a.product_id=%s
                  and a.state='done'
                  and (a.location_id=%s or a.location_dest_id=%s)
                  and a.date::date between %s and %s
                  and a.company_id=%s
            order by a.product_id,a.date
        """
        self.cr.execute(sql, (location_id,location_id,location_id,location_id, product_id, location_id, location_id, date_from, date_to,  company_id))
        data = self.cr.dictfetchall()
        return data

    def _beginning_balance(self, product_id, date_from, company_id, location_id):
        sql = """
            select
                   coalesce(sum(case when a.location_dest_id=%s then a.product_uom_qty
                        else 0
                   end) - sum(case when a.location_id=%s then a.product_uom_qty
                        else 0
                   end),0)     beginning_balance_qty,
                   coalesce(sum(a.price_unit),0) as price,
                   coalesce(sum(case when a.location_dest_id=%s then a.product_uom_qty*a.price_unit
                        else 0
                   end)- sum(case when a.location_id=%s then a.product_uom_qty*a.price_unit
                        else 0
                   end),0)     beginning_balance_value
            from stock_move 		as a
            where a.product_id=%s
                  and a.company_id=%s
                  and a.state='done'
                  and (a.location_id=%s or a.location_dest_id=%s)
                  and a.date::date < %s
        """
        self.cr.execute(sql, (location_id, location_id, location_id, location_id, product_id, company_id, location_id, location_id,date_from))
        res = self.cr.fetchone()
        return res


class report_product_stock_card(osv.AbstractModel):
    _name = 'report.product.report_stock_card'
    _inherit = 'report.abstract_report'
    _template = 'product.report_stock_card'
    _wrapped_report_class = report_stock_card

class report_product_stock_card_noprice(osv.AbstractModel):
    _name = 'report.product.report_stock_card_noprice'
    _inherit = 'report.abstract_report'
    _template = 'product.report_stock_card_noprice'
    _wrapped_report_class = report_stock_card


