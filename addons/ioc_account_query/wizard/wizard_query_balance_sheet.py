# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields

class wizard_query_balance_sheet(osv.osv_memory):
    _name = 'account.wizard_query_balance_sheet'
    _description = 'Wizard Print Balance Sheet'

    def default_company_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')

        user = obj_user.browse(cr, uid, [uid])[0]

        return user.company_id and user.company_id.id or False

    def default_fiscalyear_id(self, cr, uid, context={}):
        obj_fiscalyear = self.pool.get('account.fiscalyear')

        fiscalyear_id = obj_fiscalyear.find(cr, uid)
        
        return fiscalyear_id or False

    def default_period_id(self, cr, uid, context={}):
        obj_period = self.pool.get('account.period')

        period_ids = obj_period.find(cr, uid)

        return period_ids and period_ids[0] or False

    def default_state(self, cr, uid, context={}):
        return 'posted'

    _columns =  {
                'company_id' : fields.many2one(string='Company', obj='res.company', required=True),
                'fiscalyear_id' : fields.many2one(string='Fiscal Year', obj='account.fiscalyear', required=True),
                'period_id' : fields.many2one(string='Period', obj='account.period', required=True),
                'state' : fields.selection(string='State', selection=[('all','All'),('draft','Draft'),('posted','Posted')], required=True),
                }

    _defaults = {
                'company_id' : default_company_id,
                'fiscalyear_id' : default_fiscalyear_id,
                'period_id' : default_period_id,
                'state' : default_state,
                }

    def button_show_query(self, cr, uid, ids, data, context=None):
        obj_data = self.pool.get('ir.model.data')

        if context is None:
            context = {}

        view_id = obj_data.get_object_reference(cr, uid, 'ioc_account_query', 'list_account_queryBalanceSheet')[1]

        wizard = self.browse(cr, uid, ids)[0]
            
        
        return  {
                'type': 'ir.actions.act_window',
                'name' : 'Balance Sheet',
                'res_model' : 'account.query_balance_sheet',
                'view_type' : 'tree',
                'view_mode' : 'tree',
                'view_id' : view_id,
                'domain' : "[('parent_id','=',False)]",
                'context' : {'period_id' : wizard.period_id.id, 'fiscalyear_id' : wizard.fiscalyear_id.id, 'state' : wizard.state},
                'target' : 'current',
                }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
