# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv,fields
from openerp.tools.translate import _
from datetime import datetime

class wizard_report_profit_loss(osv.osv_memory):
    _name = 'account.wizard_report_profit_loss'
    _description = 'Wizard Report Profit Loss'

    def default_company_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')

        user = obj_user.browse(cr, uid, [uid])[0]

        return user.company_id and user.company_id.id or False

    _columns =  {
                'company_id' : fields.many2one(string='Company', obj='res.company', required=True),
                'account_id': fields.many2one(string='Analytic Account', obj='account.analytic.account'),
                'date_start': fields.date(string='Start Date', required=True),
                'date_stop': fields.date(string='End Date', required=True),
                'type': fields.selection([('product', 'By Product'),('brand','By Brand')
                                             , ('category', 'By Category')],"Report Type", required=True),
                }

    _defaults = {
                'type': 'product',
                'company_id' : default_company_id,
    }

    def button_print_report_profit_loss(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        datas = {
            'ids': context.get('active_ids', []),
            'model': 'account_move',
            'form': data
        }
        datas['form']['ids'] = datas['ids']
        return self.pool['report'].get_action(cr, uid, [], 'account.report_profit_loss', data=datas, context=context)

    def button_print_report_profit_loss_all(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        datas = {
            'ids': context.get('active_ids', []),
            'model': 'account_move',
            'form': data
        }
        datas['form']['ids'] = datas['ids']
        return self.pool['report'].get_action(cr, uid, [], 'account.report_profit_loss_all', data=datas, context=context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
