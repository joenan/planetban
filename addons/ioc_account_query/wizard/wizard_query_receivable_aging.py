# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields

class wizard_query_receivable_aging(osv.osv_memory):
    _name = 'account.wizard_query_receivable_aging'
    _description = 'Wizard Query Receivable Aging'
   
    def default_company_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')

        user = obj_user.browse(cr, uid, [uid])[0]

        return user.company_id.id

    def default_direction(self, cr, uid, context={}):
        return 'future'

    def default_period_lenght(self, cr, uid, context={}):
        return 30

    _columns =  {
                'company_id' : fields.many2one(string='Company', obj='res.company', required=True),
                'date' : fields.date(string='Start Date', required=True),
                'direction' : fields.selection(string='Direction', selection=[('past','Past'),('future','Future')], required=True),
                'period_lenght' : fields.integer(string='Period Length (days)', required=True),
                }

    _defaults = {
                'company_id' : default_company_id,
                'direction' : default_direction,
                'period_lenght' : default_period_lenght,
                }

    def button_query_report(self, cr, uid, ids, data, context=None):
        obj_data = self.pool.get('ir.model.data')
        obj_line = self.pool.get('account.move.line')
        valid_ids = []

        criteria = [('journal_id.type','=','sale'),('account_id.type','=','receivable')]
        line_ids = obj_line.search(cr, uid, criteria)

        wizard = self.browse(cr, uid, ids)[0]

        if line_ids:
            for line in obj_line.browse(cr, uid, line_ids, context={'date' : wizard.date}):
                if line.amount_residual != 0.0:
                    valid_ids.append(line.id)


        view_id = obj_data.get_object_reference(cr, uid, 'ioc_account_query', 'tree_account_queryReceivableAging')[1]
        if context is None:
            context = {}
            
        
        return  {
                'type': 'ir.actions.act_window',
                'name' : 'Receivable Aging',
                'res_model' : 'account.query_receivable_aging',
                'view_type' : 'form',
                'view_mode' : 'tree',
                'view_id' : view_id,
                'target' : 'current',
                'context' : {'period_lenght' : wizard.period_lenght, 'date' : wizard.date},
                'domain' : [('date',wizard.direction == 'future' and '>=' or '<=', wizard.date),('id','in',valid_ids)],
                }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
