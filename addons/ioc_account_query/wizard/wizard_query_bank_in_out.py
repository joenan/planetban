# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields
from openerp.tools.translate import _
from datetime import datetime

class wizard_query_bank_in_out(osv.osv_memory):
    _name = 'account.wizard_query_bank_in_out'
    _description = 'Wizard Query Bank In Out'
   
    def default_company_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')

        user = obj_user.browse(cr, uid, [uid])[0]

        return user.company_id.id

    def default_state(self, cr, uid, context={}):
        return 'posted'

    _columns =  {
                'company_id' : fields.many2one(string='Company', obj='res.company', required=True),
                'fiscalyear_id' : fields.many2one(string='Fiscal Year', obj='account.fiscalyear', required=True),
                'date_start' : fields.date(string='Start Date', required=True),
                'date_stop' : fields.date(string='End Date', required=True),
                'journal_ids' : fields.many2many(string='Journal', obj='account.journal', domain=[('type','=','bank')]),
                'state' : fields.selection(string='State', selection=[('all','All'),('draft','Draft'),('posted','Posted')], required=True),
                }

    _defaults = {
                'company_id' : default_company_id,
                'state' : default_state,
                }

    def _check_date(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids):
            date_start = datetime.strptime(wizard.date_start, '%Y-%m-%d')
            date_stop = datetime.strptime(wizard.date_stop, '%Y-%m-%d')
            fiscal_date_start = datetime.strptime(wizard.fiscalyear_id.date_start, '%Y-%m-%d')
            fiscal_date_stop = datetime.strptime(wizard.fiscalyear_id.date_stop, '%Y-%m-%d')

            if date_start < fiscal_date_start or date_stop < fiscal_date_start or date_start > fiscal_date_stop or date_stop > fiscal_date_stop:
                return False

        return True


    def button_query_report(self, cr, uid, ids, data, context=None):
        obj_data = self.pool.get('ir.model.data')

        if context is None:
            context = {}

        if not self._check_date(cr, uid, ids):
            raise osv.except_osv(_('Warning!'),_('Invalid start or end date'))
            return False
 
        wizard = self.browse(cr, uid, ids)[0]


        criteria = []

        if wizard.date_start:
            criteria += [('date','>=',wizard.date_start)]

        if wizard.date_stop:
            criteria += [('date','<=',wizard.date_stop)]

        if wizard.state != 'all':
            criteria += [('state','=',wizard.state)]

        if wizard.journal_ids:
            journal_ids = []
            for journal in wizard.journal_ids:
                journal_ids.append(journal.id)
            criteria += [('journal_id','in',journal_ids)]


        view_id = obj_data.get_object_reference(cr, uid, 'ioc_account_query', 'tree_account_queryBankInOut')[1]
            
        
        return  {
                'type': 'ir.actions.act_window',
                'name' : 'Bank In/Out',
                'res_model' : 'account.query_bank_in_out',
                'view_type' : 'form',
                'view_mode' : 'tree',
                'view_id' : view_id,
                'target' : 'current',
                'domain' : criteria,
                }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
