# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import wizard_query_trial_balance
import wizard_query_balance_sheet
import wizard_query_general_ledger
import wizard_query_income_statement
import wizard_query_cash_in_out
import wizard_query_bank_in_out
import wizard_query_sales_detail
import wizard_query_expenses_detail
import wizard_report_profit_loss
import wizard_report_stock_card
import wizard_report_inventory_mutation
import wizard_report_operation_cost
import wizard_report_cash_toko
import wizard_report_internal_transfer
import wizard_report_outstanding_po
# import wizard_query_receivable_aging


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
