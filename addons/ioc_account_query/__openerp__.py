# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name' : 'IOC Account Query',
    'version' : '1.0',
    'author' : 'Indonesian Odoo Community',
    'category' : 'Accounting',
    'summary' : 'Report Account Query',
    'description' : """
Available Query :

1. Trial Balance
2. Balance Sheet
3. General Ledger
4. Income Statement

5. Cash In Out
6. Bank In Out

7. Sales Detail
8. Expense Detail


    """,
    'website': 'www.indonesian-odoo.com',
    'images' : [],
    'depends' : [ 'account', 'account_accountant',
                ],
    'data' : [
                'account_query_report.xml',
                'security/ir.model.access.csv',
                'report/query_trial_balance.xml',
                'report/query_balance_sheet.xml',
                'report/query_general_ledger.xml',
                'report/query_income_statement.xml',
                'report/query_cash_in_out.xml',
                'report/query_bank_in_out.xml',
                'report/query_order_analyst_payment.xml',
                # 'report/query_receivable_aging.xml',
                'report/query_sales_detail.xml',
                'report/query_expenses_detail.xml',

                'wizard/wizard_query_trial_balance.xml',
                'wizard/wizard_query_balance_sheet.xml',
                'wizard/wizard_query_general_ledger.xml',
                'wizard/wizard_query_income_statement.xml',
                'wizard/wizard_query_cash_in_out.xml',
                'wizard/wizard_query_bank_in_out.xml',
                # 'wizard/wizard_query_receivable_aging.xml',
                'wizard/wizard_query_sales_detail.xml',
                'wizard/wizard_query_expenses_detail.xml',
                'wizard/wizard_report_profit_loss.xml',
                'wizard/wizard_report_stock_card.xml',
                'wizard/wizard_report_inventory_mutation.xml',
                'wizard/wizard_report_operation_cost.xml',
                'wizard/wizard_report_cash_toko.xml',
                'wizard/wizard_report_internal_transfer.xml',
                'wizard/wizard_report_outstanding_po.xml',
                'menu/menu_query.xml',
                'views/report_cash_in_out.xml',
                'views/report_profit_loss.xml',
                'views/report_profit_loss_all.xml',
                'views/report_stock_card.xml',
                'views/report_inventory_mutation.xml',
                'views/report_operation_cost.xml',
                'views/report_cash_toko.xml',
                'views/report_internal_transfer.xml',
                'views/report_outstanding_po.xml',
                 ],
    'js' : [],
    'qweb' : [],
    'css' : [],
    'demo ': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application' : True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
