
import time
from openerp.osv import osv
from openerp.report import report_sxw

class invoice_pb(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(invoice_pb, self).__init__(cr, uid, name, context=context)

        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        partner = user.company_id.partner_id

        self.localcontext.update({
            'time': time,
	    'cust_pay': self._cust_pay,
            'product_desc': self._get_product_desc,
        })

    def _cust_pay(self, inv_id):
        sql = """select a.id,coalesce(c.amount,0.00) as amount,split_part(c.name,':',2)
                    from account_invoice a
                    left join pos_order b
                    on a.reference = b.name
                    left join account_bank_statement_line c
                    on b.id = c.pos_statement_id
                    where a.id=%s
                    and coalesce(c.amount,0.00) >= 0"""
        self.cr.execute(sql, (inv_id,))
        res = self.cr.fetchone()
        return res

    def _get_product_desc(self, inv_id):
        data={}
        sql = """ select a.description_sale as product_desc
                    from product_template a
                    left outer join product_product d
                    on a.name = d.name_template
                    left outer join account_invoice_line b
                    on d.id = b.product_id
                    left outer join account_invoice c
                    on b.invoice_id = c.id
                    where a.description_sale is not null and c.id=%d"""%(inv_id)
        self.cr.execute(sql)
        data = self.cr.dictfetchall()
        return data

class report_invoice_pb(osv.AbstractModel):
    _name = 'report.account.report_invoice_pb'
    _inherit = 'report.abstract_report'
    _template = 'account.report_invoice_pb'
    _wrapped_report_class = invoice_pb
