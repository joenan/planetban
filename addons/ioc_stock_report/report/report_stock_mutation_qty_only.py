import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime
from dateutil.relativedelta import relativedelta

class stock_mutation_qty_only(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(stock_mutation_qty_only, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'time': time,
            'row_stock_mutation': self._row_stock_mutation
        })

    def _row_stock_mutation(self, location_id, date_to):
        data = {}
        sql = """
            SELECT
                brand,
                code,
                product_name,
                uom,
                state,
                sum(begining_qty)	as begining_qty,
                sum(qty_begining_qty)	as qty_begining_qty
            FROM
            (
                        select
                            h.name				as brand,
                            c.default_code			as code,
                            c.name_template			as product_name,
                            e.name				as uom,
                            case when c.active='t' then 'Active'
                                 else 'Non Active'
                            end					as state,
                            sum(case
                            when a.location_id=%s then -1 * (product_uom_qty * a.price_unit)
                            when a.location_dest_id=%s then (product_uom_qty * a.price_unit)
                                        end) as begining_qty,
                            sum(case
                            when a.location_id=%s then -1 * (product_uom_qty)
                            when a.location_dest_id=%s then product_uom_qty
                                        end) as qty_begining_qty
                        from stock_move 			as a
                            left join product_product 		as c on a.product_id=c.id
                            left join product_template		as d on c.product_tmpl_id=d.id
                            left join product_uom		as e on d.uom_id=e.id
                            left join stock_picking_type 	as f on a.picking_type_id=f.id
                            left join stock_location 		as g on a.location_id=g.id
                            left join stock_location		as b on a.location_dest_id=b.id
                            left join product_tire_brand	as h on d.pb_tire_brand=h.id
                        where
                            (a.location_id=%s or a.location_Dest_id=%s)
                            and a.state='done'
                            and a.date::date <= %s
                        group by h.name,c.default_code,c.name_template,e.name,c.active
                        ) AS data
            GROUP BY
                brand,
                code,
                product_name,
                uom,
                state
            ORDER BY
                brand,
                code,
                product_name
        """
        # Begining Balance
        #   L, L, L, L, L, L, S

        self.cr.execute(sql, (location_id, location_id, location_id, location_id, location_id, location_id, date_to,))
        data = self.cr.dictfetchall()
        return data



class report_stock_mutation_qty_only(osv.AbstractModel):
    _name = 'report.stock.stock_mutation_qty_only'
    _inherit = 'report.abstract_report'
    _template = 'stock.stock_mutation_qty_only'
    _wrapped_report_class = stock_mutation_qty_only


