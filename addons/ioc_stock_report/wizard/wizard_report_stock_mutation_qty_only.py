from openerp.osv import osv,fields
from openerp.tools.translate import _
from datetime import datetime

class wizard_report_stock_mutation_qty_only(osv.osv_memory):
    _name = 'product.wizard_report_stock_mutation_qty_only'
    _description = 'Wizard Report Stock Mutation Qty Only'

    def default_company_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')
        user = obj_user.browse(cr, uid, [uid])[0]
        return user.company_id and user.company_id.id or False

    def default_location_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')
        user = obj_user.browse(cr, uid, [uid])[0]
        return user.pos_config.stock_location_id and user.pos_config.stock_location_id.id or False

    def _default_location_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')
        user = obj_user.browse(cr, uid, [uid])[0]
        if user.pos_config.stock_location_id:
            return True
        else:
            return False

    _columns =  {
                'company_id' : fields.many2one(string='Company', obj='res.company', required=True),
                'date_to': fields.date(string='Date To', required=True),
                'location_id': fields.many2one('stock.location','WH Locaton', required=True,
                            domain="[('company_id','=',company_id)]", readonly=False),
                'fg_pos_user': fields.boolean('Flag POS',help="Flag for pos")
                }

    _defaults = {
                'location_id': default_location_id,
                'company_id': default_company_id,
                'fg_pos_user': _default_location_id
    }

    def button_print_report_stock_mutation_qty_only(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        datas = {
            'ids': context.get('active_ids', []),
            'model': 'product_product',
            'form': data
        }
        datas['form']['ids'] = datas['ids']
        return self.pool['report'].get_action(cr, uid, [], 'stock.stock_mutation_qty_only', data=datas, context=context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
