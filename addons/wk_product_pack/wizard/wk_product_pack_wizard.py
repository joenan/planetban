# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################


from openerp.osv import fields, osv
from openerp.tools.translate import _


class product_pack_wizard(osv.osv_memory):
	_name = 'product.pack.wizard'
	_columns = {
		'name':fields.text('Description'),
		'product_name':fields.many2one('product.product','Product',required=True),
		'quantity': fields.float('Quantity'),
		'unit_price':fields.float('Unit Price'),
		'delay':fields.float('Delivery Load Time'),
		'taxes':fields.many2one('account.tax','Taxes')
	}
	_defaults = {
        'quantity':1.0
        }
	def add_product_button(self, cr, uid, ids, context=None):
		if context is None:
			context = {}
		pack_id = self.browse(cr, uid, ids[0])
		self.pool.get('sale.order.line').create(cr, uid, {'order_id':context['active_id'],'product_id':pack_id.product_name.id}, context)	
		return True

	def get_unit_price_onchange(self, cr, uid, ids, product_name=False, context=None):
		if context is None:
			context = {}
		products = ""
		description=""
		product_obj = self.pool.get('product.product').browse(cr, uid, product_name)
		if product_name:
			list_price = product_obj.list_price
			if product_obj.wk_product_pack:
				for new_obj in product_obj.wk_product_pack:
					quantity = product_obj.wk_product_pack.product_quantity
					products += str(new_obj.product_name.name) + ",\t"
			else:
				products = "No product"
			if product_obj.description:
				descrpt = str(product_obj.description)
			else:
				descrpt = "No Description"
			description = "Pack Name:  " + str(product_obj.name) + "\n" + "Products:  "+ products +"\n" +"Description:\n" + "\t\t "+ descrpt
			return {'value': {'unit_price':list_price,
							  'name':description,
							  'quantity':quantity}
				   }	
product_pack_wizard()




 