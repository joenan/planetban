# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################
{
    "name": "POS Product pack",
    "category": 'point_of_sale',
    "summary": """
        POS Product pack allow us to sale products in pack.""",
    "description": """ "",

====================
**Help and Support**
====================
.. |icon_features| image:: pos_product_pack/static/src/img/icon-features.png
.. |icon_support| image:: pos_product_pack/static/src/img/icon-support.png
.. |icon_help| image:: pos_product_pack/static/src/img/icon-help.png

|icon_help| `Help <https://webkul.com/ticket/open.php>`_ |icon_support| `Support <https://webkul.com/ticket/open.php>`_ |icon_features| `Request new Feature(s) <https://webkul.com/ticket/open.php>`_
    """,
    "sequence": 1,
    "author": "Webkul Software Pvt. Ltd.",
    "website": "http://www.webkul.com",
    "version": '1.0',
    "depends": ['point_of_sale','wk_product_pack'],
    "data": [
        
        'views/pos_product_pack_js.xml',
        # 'views/pos_product_pack_view.xml',
    ],
    'qweb': [
        'static/src/xml/pos_product_pack.xml'
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
    "price": 1,
    "currency": 'EUR',
}