openerp.pos_product_pack = function(instance, module){
module = instance.point_of_sale;
_t = instance.web._t;
var round_pr = instance.web.round_precision

var PosModelSuper = module.PosModel
    module.PosModel = module.PosModel.extend({
    load_server_data: function(){
        var self = this;
        var loaded = PosModelSuper.prototype.load_server_data.call(this);        
        loaded = loaded.then(function(){                   
                    self.fetch(
                                'product.product',
                                ['id','name','wk_product_pack','lst_price'],
                                [['is_pack', '=',true]])
                        .then(function(pack_product){  
                            self.set({'product_pack': pack_product});
                    });
                   return self.fetch(
                                'product.pack',
                                ['id','product_name','product_quantity','uom_id','price','name'],
                                [])
                        .then(function(wk_pack_product){  
                            self.set({'wk_pack_product': wk_pack_product});
                    });
                    
                });
        return loaded;
        },
    });

var ProductListWidgetSuper = module.ProductListWidget
   module.ProductListWidget = module.ProductListWidget.extend({
    renderElement: function() {
            self=this;
            this._super();
            _.each(self.pos.get('product_pack'), function(pack_product){
                   $('span[data-product-id='+pack_product.id+']').append("<b style=''>Appended textdfsgdsfgdfsgdfsgdfg</b>");
            });
        },
    wk_is_pack_product:function(product_id){
        var pack_product=self.pos.get('product_pack');
        for(i=0;i<pack_product.length;i++){
            if(pack_product[i].id==product_id){
                    return true;
            }
        }
    },
    });

var  OrderlineSuper = module.Orderline 
 module.Orderline  = module.Orderline .extend({
   getPackProduct:function(pack_product_id,product_price,product_qty){
        self=this;
        var pack_product=self.pos.get('product_pack');
        var wk_pack_products=self.pos.get('wk_pack_product');
        var pack_product_list=[];
        var savedprice=0;
        for(i=0;i<pack_product.length;i++)
        {
            if(pack_product[i].id==pack_product_id && (pack_product[i].wk_product_pack).length>0 )
                {
                for(j=0;j<(pack_product[i].wk_product_pack).length;j++){    
                    for(k=0;k<wk_pack_products.length;k++){
                        if(wk_pack_products[k].id==pack_product[i].wk_product_pack[j]){
                           
                        var product_val={'display_name':wk_pack_products[k].name,'uom_id':wk_pack_products[k].uom_id,'price':wk_pack_products[k].price};
                        pack_product_list.push({'product':product_val,'qty':wk_pack_products[k].product_quantity});
                        savedprice += wk_pack_products[k].price*wk_pack_products[k].product_quantity*parseFloat(product_qty);
                        }
                    }
                }
            return {'pack_product_list':pack_product_list,'wk_pack_benefit':savedprice-product_price}; 
            }
        }
         
    },
    wk_get_unit: function(unit_id){
            if(!unit_id){
                return undefined;
            }
            return unit_id[1];
        },
    });

 var _super = module.Order;
    module.Order = module.Order.extend({
        get_pack_product_benefits: function(orderlines){
            self=this;
            var savedprice=0;
            var wk_quantity_price=0
            var pack_products=self.pos.get('product_pack')
            var wk_pack_products=self.pos.get('wk_pack_product')
            _.each(orderlines, function(orderline){
                _.each(pack_products, function(pack_product){
                    if(orderline.product.id==pack_product.id){
                        _.each(pack_product.wk_product_pack, function(pack_product_ids){
                        for(k=0;k<wk_pack_products.length;k++){
                        
                        if(wk_pack_products[k].id==pack_product_ids){
                         savedprice= savedprice +( wk_pack_products[k].price * wk_pack_products[k].product_quantity * orderline.quantity);
                            
                        }
                    }
                   
                        
                    });
                     wk_quantity_price+= orderline.quantity*orderline.product.price 
                        console.log(orderline.product);
                        
                }
                
                }); 
                 
            });
            savedprice-=wk_quantity_price;
            if(savedprice > parseFloat(0))
                return savedprice;
            return 0;
            
        },
 });
}

