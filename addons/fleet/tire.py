from openerp.osv import osv, fields

class tire(osv.osv):
	_name = 'fleet.vehicle.tire'
	_columns = {
		#'driver_id'		: fields.many2one('res.partner', 'Customer', help='Driver of the vehicle', domain="[('customer','=',True)]"),
		'vehicle_id'	: fields.many2one('fleet.vehicle', 'Vehicle', required=True),
		'date'			: fields.date('Date'),
		'shop_id'		: fields.many2one('res.partner', 'Shop', domain="[('shop','=',True)]"),
		'tipe_pembelian': fields.selection([('Pasang ditempat', 'Pasang ditempat'), ('Pasang sendiri', 'Pasang sendiri')], 'Tipe Pembelian', help='Tipe Pembelian'),
		'garansi'		: fields.selection([('3 Bulan', '3 Bulan'), ('6 Bulan', '6 Bulan')], 'Garansi', help='Garansi'),
	}