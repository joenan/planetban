from openerp.osv import osv, fields

class product_template(osv.osv):
    _inherit = "product.template"

    _columns = {
        'models_ids': fields.many2many('fleet.vehicle.model', 'products_models_rel', 'product_id', 'model_id', 'Models'),
        'vehicle_model_line': fields.one2many('product.template.vehiclemodel', 'product_id', 'Models (one2many)'),
    }
    
product_template()

class product_template_vehicle_model(osv.osv):
    _description = "Product Template Vehicle Model"
    _order = "name"
    _name = "product.template.vehiclemodel"

    _columns = {
        'name': fields.char('Model Name', size=64),
        'models_ids_one2many': fields.many2one('fleet.vehicle.model', 'Vehicle Models'),
        'note': fields.text('Notes'),
        'product_id': fields.many2one('product.template', 'Product'),
    }
    
    def oc_vehiclemodel(self, cr, uid, ids, field, context=None):
        values = {}
        if field:
            obj = self.pool.get('fleet.vehicle.model').browse(cr, uid, field, context=context)
            values = {
                      'name': obj.name,
                    }
        else :
            values = {
                      'name': False,
                    }
        return {'value': values}
    
product_template_vehicle_model()

class product_attribute_value(osv.osv):
    _inherit = "product.attribute.value"
    
    _columns = {
        'model_id': fields.many2one('fleet.vehicle.model', 'Vehicle Model'),
    }
    
    def oc_vehiclemodel(self, cr, uid, ids, field, context=None):
        values = {}
        if field:
            obj = self.pool.get('fleet.vehicle.model').browse(cr, uid, field, context=context)
            values = {
                      'name': obj.name,
                    }
        else :
            values = {
                      'name': False,
                    }
        return {'value': values}
    
product_attribute_value()

