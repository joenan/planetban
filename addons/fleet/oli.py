from openerp.osv import osv, fields

class oil(osv.osv):
	_name = 'fleet.vehicle.oil'
	_columns = {
		#'driver_id'		: fields.many2one('res.partner', 'Customer', help='Driver of the vehicle', domain="[('customer','=',True)]"),
		'vehicle_id'	: fields.many2one('fleet.vehicle', 'Vehicle', required=True),
		'date'			: fields.date('Date'),
		'shop_id'		: fields.many2one('res.partner', 'Shop', domain="[('shop','=',True)]"),
		'periode_ganti'	: fields.selection([('1 bulan', '1 bulan'), ('2 bulan', '2 bulan'), ('3 bulan', '3 bulan'), ('4 bulan', '4 bulan'), ('5 bulan', '5 bulan'), ('6 bulan', '6 bulan')], 'Period', help='Period'),
	}