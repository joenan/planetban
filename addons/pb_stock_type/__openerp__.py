{
    'name': 'Planet Ban Stock Picking Type Additional',
    'category': 'Planet Ban',
    'summary': 'Add to Stock Picking Type with Field Movement',
    'version': '1.0',
    'description': """Add to Stock Picking Type with Field Movement""",
    'author': 'Indonesian Odoo Community',
    'depends': ['base','stock'],
    'update_xml': [
        'stock_picking_type_view.xml',
    ],
    'installable': True,
    'active': False,
}
