from openerp.osv import fields, osv

class stock_picking_type(osv.Model):

    _name = 'stock.picking.type'
    _inherit = 'stock.picking.type'

    _columns = {
        'movement': fields.selection([
                                         ('purchase', 'Purchase'),
                                         ('transfer in', 'Transfer in'),
                                         ('sales refund', 'Sales Refund'),
                                         ('others in', 'Others In'),
                                         ('sales', 'Sales'),
                                         ('transfer out', 'Transfer Out'),
                                         ('purchase refund', 'Purchase Refund'),
                                         ('others out', 'Others Out')],
                                         'Movement'),
    }
