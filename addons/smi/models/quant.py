from openerp import models, fields, api
import logging
_logger = logging.getLogger(__name__)
class quant(models.Model):
    _name = 'smi.quant'
    _description = 'Stock mutation for int.transfer SMI'
    location_id = fields.Many2one('stock.location', string = 'Location', readonly = True)
    product_id = fields.Many2one('product.product', string = 'Product', readonly = True)
    qty = fields.Float(string = 'Quantity',readonly = True)
    qty_alloc = fields.Float(string = 'Allocation', readonly = True)
    qty_bal = fields.Float(string = 'Balance', readonly = True)
    note = fields.Char(string = 'Note', readonly = True)
    
    def cron_lod_prod(self):
        sql = ('truncate smi_quant restart identity;'
               'select distinct location_id, product_id, qty from stock_quant order by location_id;')
        self.env.cr.execute(sql)
        res = self.env.cr.dictfetchall()
        return res
    
    @api.model
    def cron_job_smi_quant(self):
        lot_prod_ids = self.cron_lod_prod()
        for lot_prod in lot_prod_ids:
            qty = 0
            for recq_id in self.env['stock.quant'].search([('company_id','=',1),'&',
                                                           ('location_id','=',lot_prod['location_id']),
                                                           ('product_id','=',lot_prod['product_id'])]):
                qty += recq_id.qty
            alc = 0
            for rec_id in self.env['stock.move'].search([('company_id', '=',1),'&',
                                                         ('location_id','=',lot_prod['location_id']),
                                                         ('product_id','=',lot_prod['product_id']),'&',
                                                         ('source_trans','=','internal'),
                                                         ('state','in',('draft','assigned','confirmed'))]):
                alc += rec_id.product_qty
            lot_prod['qty_alloc'] = alc
            lot_prod['qty'] = qty - alc
            lot_prod['note'] = 'cron_job'
            self.env['smi.quant'].create(lot_prod)
        _logger.info('Cron Job SMI Quant has been run')