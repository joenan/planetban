from openerp import models, fields, api, exceptions
class thsp(models.Model):
    _name = 'smi.thsp'
    _description ='Header SP'
    name = fields.Char(string = 'No.Draft')
    source_id = fields.Char(string = 'Source', required = True)
    source_name = fields.Char(compute = 'get_wh_name', store = True)
    dest_id = fields.Char(string = 'Destination', required = True)
    dest_name = fields.Char(compute = 'get_wh_name', store = True)
    date_scedule = fields.Date(string = 'Scheduled Date', required = True, default = fields.Date.today())
    date_exp = fields.Date(string = 'Expired Date', required = True)
    reff = fields.Char(string = 'Reference')
    note = fields.Text(string = 'Note')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('validate', 'Validate'),
            ('done', 'Done'),
            ], string = "State")
    tdsp_ids = fields.One2many('smi.tdsp','thsp_ids', ondelete = 'cascade', string = 'Items', required = True)
    
    @api.depends('source_id','dest_id')
    def get_wh_name(self):
        for rec in self:
            rec.source_name = self.get_wh_id(rec.source_id).name
            rec.dest_name = self.get_wh_id(rec.dest_id).name
    
    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('smi.thsp')
        vals['state'] = 'draft'
        return super(thsp, self).create(vals)
    
    def get_wh_id(self, vals):
        return self.env['stock.warehouse'].search(['&',('code','=',vals),('company_id','=',1)])
    
    def get_location_id(self, vals):
        location = self.env['stock.warehouse'].search(['&',('code','=',vals),('company_id','=',1)]).lot_stock_id
        return location.id
    
    def get_picking_type_id(self,vals):
        picking_type = self.env['stock.warehouse'].search(['&',('code','=',vals),('company_id','=',1)]).int_type_id
        return picking_type.id
    
    def get_product(self,vals):
        product_id = self.env['product.product'].search(['&',('default_code','=',vals),('company_id','=',1)])
        return product_id
    
    def get_uom(self,vals):
        uom = self.env['product.uom'].search([('name','=',vals)])
        return uom
    
    def get_stock_quant(self, product_id, int_loc_id):
        stock_quant = self.env['smi.quant'].search(['&',('location_id', '=', int_loc_id), ('product_id', '=', product_id)], 
                                                   order = 'write_date desc', limit=1)
        return stock_quant
    
    def get_stock_quant_qty(self, location_id, product_id):
        qty = 0
        for recq_id in self.env['stock.quant'].search([('company_id','=',1),'&',
                                                       ('location_id','=',location_id),
                                                       ('product_id','=', product_id)]):
            qty += recq_id.qty
        return qty
    
    def get_stock_quant_total_alloc(self, location_id, product_id):
        alc = 0
        for rec_id in self.env['stock.move'].search([('company_id', '=',1),'&',
                                                     ('location_id','=',location_id),
                                                     ('product_id','=',product_id),'&',
                                                     ('source_trans','=','internal'),
                                                     ('state','in',('draft','assigned','confirmed'))]):
            alc += rec_id.product_qty
        return alc
    
    def get_stock_quant_ballance(self, location_id, product_id):
        qty = self.get_stock_quant_qty(location_id, product_id)
        alc = self.get_stock_quant_total_alloc(location_id, product_id)
        return qty - alc
        
    def validate(self, opt):
        if opt == 'one':
            draft_sp = self
        elif opt == 'all' :
            draft_sp = self.search([('state', '=', 'draft')])
        else:
            draft_sp ={}
        for sp in draft_sp:
            for ob in self.env['smi.tdsp'].search([('thsp_ids','=',sp.id)]):
                rec_count = self.env['smi.tdsp'].search(['&', ('sku','=',ob.sku), ('thsp_ids','=',sp.id)])
                if len(rec_count) > 1:
                    raise exceptions.Warning('Too many SKU %s in %s'%(ob.sku, sp.name))
            int_loc_id = self.get_location_id(sp.source_id); int_dest_id = self.get_location_id(sp.dest_id)
            vals_move = []; rec_move = []; rec_quant = []; i = 0
            vals = {'priority': '1', 'move_type': 'one', 'company_id': 1, 'state': 'draft', 
                    'transfer_type': 'internal', 'source_trans':'internal', }
            if sp.reff != False:  
                vals['origin'] = sp.reff
            if sp.note !=False:
                vals['note'] = sp.note 
            vals['picking_type_id'] = self.get_picking_type_id(sp.source_id)
            vals['min_date'] = sp.date_scedule
            vals['max_date'] =  sp.date_exp
            vals['internal_location_id'] = int_loc_id
            vals['internal_location_dest_id'] = int_dest_id
            
            for a in self.env['smi.tdsp'].search([('thsp_ids','=',sp.id)]):
                product = self.get_product(a.sku);
                #quant_qty = self.get_stock_quant_qty(int_loc_id, product_id)
                #quant_alloc = self.get_stock_quant_total_alloc(int_loc_id, product_id)
                #quant_ballance = self.get_stock_quant_ballance(int_loc_id, product.id)
                #if quant_ballance >= a.qty:
                rec_vals = {'date_expected': sp.date_scedule, 'location_id': int_loc_id, 'location_dest_id': int_dest_id, 
                            'product_id': product.id, 'name': product.name_template, 'product_uom_qty': a.qty, 
                            'product_uos_qty': a.qty, 'origin_qty': a.qty, }
                uom_uos = self.get_uom(a.uom).id 
                rec_vals['product_uom'] = uom_uos
                rec_vals['product_uos'] = uom_uos
                rec_move += [(0, i, rec_vals)]
                i = i + 1
                #else:
                    #raise exceptions.Warning('Stock(%s) SKU %s is insufficient in %s'%(sp.source_id, a.sku, sp.name))
            if len(rec_move) == 0:
                raise exceptions.Warning('Items in Draft SP(%s) is empty'%(sp.name))
            else:
                vals['move_lines']= rec_move
                stock_picking = self.env['stock.picking']
                stock_picking.create(vals)
                sp.write({'state': 'done',})

    @api.multi
    @api.constrains('source_id','dest_id','date_exp','date_scedule')
    def check_constrains(self):
        self.ensure_one()
        if not self.get_wh_id(self.source_id):
            raise exceptions.ValidationError('Source %s not available in warehouse list'%self.source_id)
        elif not self.get_wh_id(self.dest_id):
            raise exceptions.ValidationError('Destination %s not available in warehouse list'%self.dest_id)
        elif self.source_id == self.dest_id:
            raise exceptions.ValidationError('Source and destination must be different')
        elif self.date_exp <= self.date_scedule:
            raise exceptions.ValidationError('Please check expired date')
        
    @api.multi
    def run_cron_job(self):
        return self.env['smi.quant'].cron_job_smi_quant()
    
    @api.one
    def draft_validate(self):
        self.validate('one')
        
    @api.multi
    def draft_validate_all(self):
        self.validate('all')
