from openerp import models, fields, api, exceptions
class tdsp(models.Model):
    _name = 'smi.tdsp'
    _description = 'Details SP'
    thsp_ids = fields.Many2one('smi.thsp', string = 'Items')
    sku = fields.Char(string = 'SKU', required = True)
    sku_desc = fields.Char(string = 'Description', compute = 'get_sku_desc',)
    uom = fields.Char(string = 'UOM', required = True, default = 'PCS')
    qty = fields.Integer(string = 'Quantity', onchange = 'check_qty', required = True)
    
    @api.onchange('qty')
    def check_qty(self):
        if self.qty < 0 :
            self.qty = ''
            
    @api.depends('sku')
    def get_sku_desc(self):
        for rec in self:
            rec.sku_desc = self.env['product.product'].search(['&',('default_code','=',rec.sku), 
                                                               ('company_id','=',1)], limit = 1).name
    
    @api.multi
    @api.constrains('sku','qty','uom')
    def check_items(self):
        self.ensure_one()
        if not self.env['product.product'].search([('default_code','=',self.sku)]):
            raise exceptions.ValidationError('SKU %s not found in product list'%self.sku)
        elif not self.qty > 0 :
            raise exceptions.ValidationError('Qty %s not allowed'%self.qty)
        elif not self.env['product.uom'].search([('name','=',self.uom)]):
            raise exceptions.ValidationError('UOM %s not allowed'%self.uom)