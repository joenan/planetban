{
'name':'Planet Ban',
'version':'1.0',
'author':'SMI Project',
'category':'Hidden',
'summary':'Custom module for internal use.',
'description': """
===============================
""",
'depends':['stock'],
'data':['security/smi_security.xml',
        'security/ir.model.access.csv',
        'data/tree.xml',
        'data/form.xml',
        'data/actions.xml',
        'data/sequence.xml',
        'data/smi_view.xml',
        'data/cron_job.xml',
        'views/tdsp.xml',
        'views/thsp.xml',
        'views/draftsp.xml'
        ],
'license': 'Other proprietary',
'application': True,
'auto_install':False,
}