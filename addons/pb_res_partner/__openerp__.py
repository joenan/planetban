{
    'name': 'Planet Ban Res Custom Partners',
    'category': 'Planet Ban',
    'summary': 'Additional Field for Res Partner',
    'version': '1.0',
    'description': """Transfer Payment Acquirer""",
    'author': 'Indonesian Odoo Community',
    'depends': ['base','fleet'],
    'update_xml': [
        'res_partner_view.xml',
    ],
    'installable': True,
    'active': False,
}
