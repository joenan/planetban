__author__ = 'dham'

from openerp.osv import fields, osv

class res_partner(osv.Model):

    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'nopol': fields.char('Nomor Polisi'),
	'pb_barcode': fields.char('No. Apung (Kartu)',size=13),
	'model_id': fields.many2one('fleet.vehicle.model', 'Model Kendaraan', help='Model of the vehicle'),
    }
