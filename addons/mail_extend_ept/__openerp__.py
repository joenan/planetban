{
    'name': 'Mail Wall Extension',
    'author': 'Hiren Vora - Emipro Technologies',
    'website': 'http://www.emiprotechnologies.com',
    'category': 'Social Network',
    'version': '1.0',
    'description':
        """
By Installing this module user will able to compose new email from Inbox.

By default in V8, compose new email button is hidden or removed. With this module that button will be again visible.

Contact : info@emiprotechnologies.com for any kind of services on Odoo V8.
        """,
    'depends': ['mail'],
    'auto_install': False,
    'qweb' : [
        "static/src/xml/*.xml",
    ],
}