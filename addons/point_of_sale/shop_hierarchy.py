import logging
import time

from openerp import tools
from openerp.osv import fields, osv
from openerp.tools import float_is_zero
from openerp.tools.translate import _

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from dateutil.relativedelta import relativedelta
from datetime import datetime

_logger = logging.getLogger(__name__)

class pos_shop(osv.osv):
    _name = 'pos.shop'
    _order = 'name'

    _columns = {
        'name' : fields.char('Shop Name', required=True),
        'shop_id': fields.many2one('res.partner', 'Shop', domain=[('shop', '=', True)], required=True),
        'ownership': fields.selection([('Franchise', 'Franchise'),('Reguler', 'Reguler')], 'Ownership', required=True),
        'config_id': fields.many2one('pos.config', 'POS Config', required=True),
        
        'pos_region_id': fields.many2one('pos.shop.region', 'Region'),
        'pos_area_id': fields.many2one('pos.shop.area', 'Area'),
        'pos_type_id': fields.many2one('pos.shop.type', 'Type'),
        
        'region_head_id': fields.many2one('hr.employee', 'Region Head'),
        'area_head_id': fields.many2one('hr.employee', 'Area Coordinator'),
    }

    _defaults = {
    }

    _sql_constraints = [
        ('uniq_name', 'unique(config_id)', "The POS Config Already Exist !"),
    ]

    def oc_shop(self, cr, uid, ids, field, context=None):
        values = {}
        if field:
            obj = self.pool.get('res.partner').browse(cr, uid, field, context=context)
            values = {
                      'name': obj.name,
                    }
        return {'value': values}


class pos_shop_region(osv.osv):
    _name = 'pos.shop.region'
    _order = 'name'

    _columns = {
        'name' : fields.char('Region Name', required=True),
    }

    _defaults = {
    }

    _sql_constraints = [
        ('uniq_name', 'unique(name)', "The name Already Exist !"),
    ]

class pos_shop_area(osv.osv):
    _name = 'pos.shop.area'
    _order = 'name'

    _columns = {
        'name' : fields.char('Area Name', required=True),
    }

    _defaults = {
    }

    _sql_constraints = [
        ('uniq_name', 'unique(name)', "The name Already Exist !"),
    ]

class pos_shop_type(osv.osv):
    _name = 'pos.shop.type'
    _order = 'name'

    _columns = {
        'name' : fields.char('Type Name', required=True),
    }

    _defaults = {
    }

    _sql_constraints = [
        ('uniq_name', 'unique(name)', "The name Already Exist !"),
    ]

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
