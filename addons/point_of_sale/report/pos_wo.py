
import time
from openerp.osv import osv
from openerp.report import report_sxw

class pos_wo(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(pos_wo, self).__init__(cr, uid, name, context=context)

        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        partner = user.company_id.partner_id

        self.localcontext.update({
            'time': time,
	        'no_antrian': self._no_antrian,
        })

    def _no_antrian(self, pos_reference):
        no_antrian = ''
        if pos_reference:
            no_antrian = pos_reference[-4:]
        return no_antrian

class report_pos_wo(osv.AbstractModel):
    _name = 'report.point_of_sale.report_pos_wo'
    _inherit = 'report.abstract_report'
    _template = 'point_of_sale.report_pos_wo'
    _wrapped_report_class = pos_wo
